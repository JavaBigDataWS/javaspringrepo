package com.javatpoint.microservices.netflixzuulapigatewayserver;

import brave.sampler.Sampler;
import org.springframework.context.annotation.Bean;

import com.netflix.hystrix.Hystrix;

import javax.annotation.PreDestroy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;


@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient

public class NetflixZuulApiGatewayServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(NetflixZuulApiGatewayServerApplication.class, args);
	}
	
	 @PreDestroy
	  public void cleanUp() {
	    Hystrix.reset();
	  }
	 

	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}
}