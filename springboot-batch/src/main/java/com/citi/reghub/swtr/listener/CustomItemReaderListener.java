package com.citi.reghub.swtr.listener;


import org.springframework.batch.core.ItemReadListener;
import org.springframework.stereotype.Component;
import com.citi.reghub.swtr.model.Entity;

@Component
public class CustomItemReaderListener implements ItemReadListener<Entity> {
	
	@Override
	public void beforeRead() {
		System.out.println("ItemReadListener - beforeRead");
	}

	@Override
	public void afterRead(Entity Entity) {
		
	}

	@Override
	public void onReadError(Exception ex) {
		// TODO Auto-generated method stub
		
	}

}