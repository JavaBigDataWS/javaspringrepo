package com.citi.reghub.swtr.kafka;

import java.util.Properties;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.citi.reghub.swtr.model.Entity;

public class SimpleProducer {

	public static void main(String[] args) throws Exception {

		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("acks", "all");
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "com.citi.reghub.swtr.model.EntityKafkaSerializer");
		Producer<String, Entity> producer = new KafkaProducer<String, Entity>(props);

		for (int i = 0; i < 1; i++) {
			Entity entity = new Entity();
			entity.setCfi("CFI222");
			entity.setIsin("ISIN222");
			entity.setParty("PARTY111");
			entity.setSourceid("SOURCEID111");
			entity.setStatus("NA");
			entity.setTrading("TRADING222");


			producer.send(new ProducerRecord<String, Entity>("swtr-csheq-inbound", "Key" + 1, entity));
			System.out.println("Message sent successfully");

		}

		producer.close();
	}
}
