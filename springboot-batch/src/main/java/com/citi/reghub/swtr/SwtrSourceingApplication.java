package com.citi.reghub.swtr;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
* @author	 	Ak96084
* @param	
* @Descrition	Will Bootstrap the Spring Boot Application
* @version
*/
@SpringBootApplication
@EnableBatchProcessing
public class SwtrSourceingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwtrSourceingApplication.class, args);
	}
}
