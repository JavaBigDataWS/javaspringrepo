package com.citi.reghub.swtr.kafka;

import java.util.Properties;
import java.util.Arrays;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import com.citi.reghub.swtr.model.Entity;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.ConsumerRecord;

public class SimpleConsumer {
  public static void main(String[] args) throws Exception {
     
     Properties props = new Properties();
     props.put("bootstrap.servers", "localhost:9092");
     props.put("group.id", "swtr");
     props.put("enable.auto.commit", "true");
     props.put("auto.commit.interval.ms", "1000");
     props.put("session.timeout.ms", "30000");
     props.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
     props.put("value.deserializer", "com.citi.reghub.swtr.model.EntityKafkaSerializer");
     @SuppressWarnings("resource")
	 KafkaConsumer<String, Entity> consumer = new KafkaConsumer<String, Entity>(props);
     consumer.subscribe(Arrays.asList("swtr-csheq-inbound"));
    
     for (int j = 0; j <2; j++) {
        @SuppressWarnings("deprecation")
		ConsumerRecords<String, Entity> records = consumer.poll(100);
        for (ConsumerRecord<String, Entity> record : records)
        System.out.printf("******************"+ record.value());
     }
  }
}
