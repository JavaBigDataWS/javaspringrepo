package com.citi.reghub.swtr.annotation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import com.citi.reghub.swtr.validator.CfiValidator;

/**
 * @author Ak96084
 * @param Exception code
 * @Descrition Generating the Field level validation annotation
 * @version
 */

@Target({ METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = CfiValidator.class)
@Documented
public @interface CfiValidation {

	String message() default "{swtr_trade_invalid_Cfi}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}