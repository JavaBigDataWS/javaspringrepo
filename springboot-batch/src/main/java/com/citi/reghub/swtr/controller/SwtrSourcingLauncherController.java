package com.citi.reghub.swtr.controller;
 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
@RestController
public class SwtrSourcingLauncherController {
 
	@Autowired
	JobLauncher jobLauncher;
 
	@Autowired
	Job job;
	
	@RequestMapping("/sourceJob/{flow}")
	public String handle(@PathVariable (name="flow") String flow) throws Exception {
 
		Logger logger = LoggerFactory.getLogger(this.getClass());
		try {
			launchJob(flow);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return "Done";
	}

	/*@Async
	@Scheduled(fixedDelay=100)
	public String scheduledJob() throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		launchJob("csheq");
		return null;
		
	}*/
	private void launchJob(String flow) throws JobExecutionAlreadyRunningException, JobRestartException,
			JobInstanceAlreadyCompleteException, JobParametersInvalidException {
			JobParameters jobParameters = new JobParametersBuilder()
				.addString("startId", "111")
				.addString("flow", flow)
				.addString("oceanQuery", "select * from swtr_trade where startId > ?")
				.addLong("time", System.currentTimeMillis()).toJobParameters();
				jobLauncher.run(job, jobParameters);
	}
}