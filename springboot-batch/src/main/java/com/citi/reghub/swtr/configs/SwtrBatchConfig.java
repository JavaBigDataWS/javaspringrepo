package com.citi.reghub.swtr.configs;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileFooterCallback;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.kafka.KafkaItemReader;
import org.springframework.batch.item.kafka.builder.KafkaItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.validation.Validator;

import com.citi.reghub.swtr.listener.CustomItemProcessotListener;
import com.citi.reghub.swtr.listener.CustomJobListener;
import com.citi.reghub.swtr.listener.CustomStepListener;
import com.citi.reghub.swtr.model.Entity;
import com.citi.reghub.swtr.model.EntityKafkaSerializer;

@Configuration
public class SwtrBatchConfig {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	public CustomStepListener customStepListener;
	
	@Autowired
	public CustomJobListener customJobListener;

	@Autowired
	public DataSource dataSource;

	@Autowired
	public Validator validator;
	
	@Autowired
	public SwtrEntityProcesser entityProcesser;
	
	@Autowired
	public SwtrCustomSkipPolicy customSkipPolicy;

	@Autowired
	public CustomItemProcessotListener customItemProcessotListener;

	@Autowired
	private SwtrPropertiesConfig swtrPropertiesConfig;
	
	@Autowired
	public SwtrSourceQuerySetter swtrSourceQuerySetter;
	
	@Autowired
	public FlatFileFooterCallback footerCallback;
	
	@Bean
	public Job job() {
		return jobBuilderFactory.get("job")
				.incrementer(new RunIdIncrementer())
				.flow(step1())
				.end()
				.listener(customJobListener)
				.build();
	}

	@Bean
	public Step step1() {
		return stepBuilderFactory.get("step1").<Entity, Entity>chunk(swtrPropertiesConfig.getReaderChunckSize())
				.reader(databaseXmlItemReader(null,null))
				//.reader(kafkaItemReader())
				.processor(entityProcesser)
				//.listener(customItemProcessotListener)  It is not working need to check
				.writer(flatFileItemWriter())
				.faultTolerant()
				.skipPolicy(customSkipPolicy)
				.listener(customStepListener)
				.build();
	}
	
	
	
	/**
	 * @author Ak96084
	 * @return  @StepScope @Value("#{jobParameters['startId']}") String  startId
	 */
	@Bean
	@StepScope
	public JdbcCursorItemReader<Entity> databaseXmlItemReader(@Value("#{jobParameters['startId']}") String  startId,@Value("#{jobParameters['oceanQuery']}") String  oceanQuery) {
		System.out.println("********"+oceanQuery);
		System.out.println("********"+startId);
		JdbcCursorItemReader<Entity> databaseReader = new JdbcCursorItemReader<>();
		databaseReader.setDataSource(dataSource);
		databaseReader.setFetchSize(swtrPropertiesConfig.getReaderFetchSize());
		databaseReader.setVerifyCursorPosition(false);
		databaseReader.setSql("select * from swtr_trade where startId > ?");
		databaseReader.setRowMapper(new BeanPropertyRowMapper<>(Entity.class));
		databaseReader.setPreparedStatementSetter(swtrSourceQuerySetter);
		return databaseReader;
	}

	@Bean
	@StepScope
	public FlatFileItemWriter<Entity> flatFileItemWriter() {
		FlatFileItemWriter<Entity> writer = new FlatFileItemWriter<>();
		Resource outputResource = new FileSystemResource(swtrPropertiesConfig.getFilePath());
		String exportFileHeader = swtrPropertiesConfig.getHeader();
		SwtrReportHeaderWriter headerWriter = new SwtrReportHeaderWriter(exportFileHeader);
		writer.setHeaderCallback(headerWriter);
		writer.setFooterCallback(footerCallback);
		writer.setResource(outputResource);
		writer.setAppendAllowed(true);
		writer.setLineAggregator(new DelimitedLineAggregator<Entity>() {
			{
				setDelimiter(",");
				setFieldExtractor(new BeanWrapperFieldExtractor<Entity>() {
					{
						setNames( swtrPropertiesConfig.getBodyParameter().split(","));
					}
				});
			}
		});
		return writer;
	}
	
	@Bean
	@StepScope
	public FlatFileFooterCallback getFooterCallback(@Value("#{stepExecution}")  final StepExecution context) {
	    
		return new FlatFileFooterCallback() {
	        
	    	@Override
	        public void writeFooter(Writer writer) throws IOException {
	            writer.append(SwtrConstant.TRADECOUNT).append(String.valueOf(context.getWriteCount()));
	        }
	    };
	}
	
	
	 /* 
	@Bean
	 public Properties consumerConfigs() {
		    Properties  props = new Properties();
	        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
	        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
	        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, swtrPropertiesConfig.getSerializer());
	        props.put(ConsumerConfig.GROUP_ID_CONFIG, "swtr");
	        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "5");
	        return props;
	    }
	    
    @Bean
	 public KafkaItemReader<String,Entity> kafkaItemReader() {
			return new KafkaItemReaderBuilder<String, Entity>()
				.partitions(0)
				.consumerProperties(consumerConfigs())
				.name("customers-reader")
				.saveState(true)
				.topic("swtr-csheq-inbound")
				.build();
		}*/
}