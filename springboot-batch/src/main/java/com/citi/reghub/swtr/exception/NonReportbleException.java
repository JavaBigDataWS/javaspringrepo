package com.citi.reghub.swtr.exception;

public class NonReportbleException extends Exception {

	private static final long serialVersionUID = 1L;

	public NonReportbleException(String s) {
		super(s);
	}
	
	public NonReportbleException() {
	}

}
