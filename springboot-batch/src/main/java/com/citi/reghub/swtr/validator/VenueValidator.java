package com.citi.reghub.swtr.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.citi.reghub.swtr.annotation.VenueValidation;

/**
* @author	 	Ak96084
* @param	    Exception code
* @Descrition	Implementation of Annotation logic
* @version
*/

public class VenueValidator implements ConstraintValidator<VenueValidation, String> {

	@Override
	public boolean isValid(String venue, ConstraintValidatorContext arg1) {

		if (venue.equalsIgnoreCase("TRADING222")) {
			
			return true;
		} else {
			
			return false;
		}
	}
}