package com.citi.reghub.swtr.configs;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import org.springframework.batch.item.validator.ValidationException;
import org.springframework.batch.item.validator.Validator;
import org.springframework.beans.factory.InitializingBean;

import com.citi.reghub.swtr.model.Entity;

public class SwtrBeanValidator implements Validator<Entity>, InitializingBean {

	private javax.validation.Validator validator;

	@Override
	public void afterPropertiesSet() throws Exception {
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.usingContext().getValidator();
	}

	@Override
	public void validate(Entity person) throws ValidationException {
		Set<ConstraintViolation<Object>> constraintViolations = validator.validate(person);

		if (constraintViolations.size() > 0) {
			generateValidationException(constraintViolations);
		}

	}

	private void generateValidationException(Set<ConstraintViolation<Object>>   constraintViolations) {
            StringBuilder message = new StringBuilder();

            for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
                message.append(constraintViolation.getMessage() + "\n");
            }

            throw new ValidationException(message.toString());
}
}
