package com.citi.reghub.swtr.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.citi.reghub.swtr.annotation.IsinValidation;

/**
* @author	 	Ak96084
* @param	    Exception code
* @Descrition	Implementation of Annotation logic
* @version
*/
public class IsinValidator implements ConstraintValidator<IsinValidation, String> {

	@Override
	public boolean isValid(String isin, ConstraintValidatorContext arg1) {

		if (isin.equalsIgnoreCase("ISIN222")) {
			
			return true;
		} else {
			
			return false;
		}
	}
}