package com.citi.reghub.swtr.configs;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Component;

@Component
public class SwtrSourceQuerySetter implements PreparedStatementSetter{
	
	private String startId;
	
	public String getStartId() {
		return startId;
	}

	public void setStartId(String startId) {
		this.startId = startId;
	}

	@Override
	public void setValues(PreparedStatement statement) throws SQLException {
		statement.setInt(1, 102);
		
	}

}
