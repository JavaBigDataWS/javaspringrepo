package com.citi.reghub.swtr.listener;


import javax.batch.api.chunk.listener.ItemProcessListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.citi.reghub.swtr.exception.NonReportbleException;
import com.citi.reghub.swtr.model.Entity;
import com.citi.reghub.swtr.repository.SwtrEntityRepository;

@Component
public class CustomItemProcessotListener implements ItemProcessListener{
	
	@Autowired
	private  SwtrEntityRepository swtrEntityRepository;
	
	@Override
	public void afterProcess(Object item, Object result) throws Exception {
		System.out.println("CustomItemProcessotListener - Started");
		Entity  e=(Entity)item;
		swtrEntityRepository.save((Entity)item);
		if(e.getReasonCode()!=null && e.getReasonCode().size()>0) {
		  throw new NonReportbleException();
		}
		throw new NonReportbleException();
		
	}

	@Override
	public void beforeProcess(Object arg0) throws Exception {
		System.out.println("CustomItemProcessotListener - Stopped");
		
	}

	@Override
	public void onProcessError(Object arg0, Exception arg1) throws Exception {
		arg1.printStackTrace();
		
	}

}