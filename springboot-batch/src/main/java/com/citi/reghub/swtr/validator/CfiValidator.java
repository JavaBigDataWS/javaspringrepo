package com.citi.reghub.swtr.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.citi.reghub.swtr.annotation.CfiValidation;

/**
* @author	 	Ak96084
* @param	    Exception code
* @Descrition	Implementation of Annotation logic
* @version
*/
public class CfiValidator implements ConstraintValidator<CfiValidation, String> {

	@Override
	public boolean isValid(String cficode, ConstraintValidatorContext arg1) {

		if (cficode.equalsIgnoreCase("CFI222")) {
			
			return true;
		} else {
			
			return false;
		}
	}
}