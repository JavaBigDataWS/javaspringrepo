package com.citi.reghub.swtr.configs;

public class SwtrConstant {

	protected static final String REPORTABLE ="REPORTABLE";
	
	protected static final String NONREPORTABLE="NONREPORTABLE";
	
	protected static final String TRADECOUNT="Total Swtr Reportable Trade count: ";
	
}
