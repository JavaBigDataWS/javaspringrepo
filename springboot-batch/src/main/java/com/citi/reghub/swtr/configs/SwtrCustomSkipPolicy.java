package com.citi.reghub.swtr.configs;

import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.stereotype.Component;
import com.citi.reghub.swtr.exception.NonReportbleException;

@Component
public class SwtrCustomSkipPolicy implements SkipPolicy {

	@Override
	public boolean shouldSkip(Throwable throwable, int skipCount) throws SkipLimitExceededException {
		
		if (throwable instanceof NonReportbleException) {
			
			return true;
		}
		return false;
	}

}
