package com.citi.reghub.swtr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.citi.reghub.swtr.model.Entity;
import com.citi.reghub.swtr.repository.SwtrEntityRepository;

@RestController
@RequestMapping(value = "/")
public class EntityRestController {

	@Autowired
	private SwtrEntityRepository swtrEntityRepository;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public Entity addNewUsers(@RequestBody Entity department) {
		return swtrEntityRepository.save(department);

	}
	
}