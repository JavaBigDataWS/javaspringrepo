package com.citi.reghub.swtr.configs;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("swtr")
public class SwtrPropertiesConfig {
	
	private String filePath;
	
	private int readerChunckSize;
	
	private int readerFetchSize;
	
	private String header;
	
	private String serializer;
	
	private String bodyParameter;

	public String getSerializer() {
		return serializer;
	}

	public void setSerializer(String serializer) {
		this.serializer = serializer;
	}

	public int getReaderFetchSize() {
		return readerFetchSize;
	}

	public void setReaderFetchSize(int readerFetchSize) {
		this.readerFetchSize = readerFetchSize;
	}
	
	public int getReaderChunckSize() {
		return readerChunckSize;
	}

	public void setReaderChunckSize(int readerChunckSize) {
		this.readerChunckSize = readerChunckSize;
	}

	public String getBodyParameter() {
		return bodyParameter;
	}

	public void setBodyParameter(String bodyParameter) {
		this.bodyParameter = bodyParameter;
	}
	
	public String getbodyParameter() {
		return bodyParameter;
	}

	public void setbodyParameter(String bodyParameter) {
		this.bodyParameter = bodyParameter;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
}