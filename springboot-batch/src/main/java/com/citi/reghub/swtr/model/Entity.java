package com.citi.reghub.swtr.model;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.citi.reghub.swtr.annotation.CfiValidation;
import com.citi.reghub.swtr.annotation.IsinValidation;
import com.citi.reghub.swtr.annotation.VenueValidation;

@Document(collection = "Entity")
public class Entity {
	
	@Id
	private String id;
	
	@IsinValidation
	private String isin;
	
	@CfiValidation
	private String cfi;
	
	private String trading;
	
	private String party;
	
	private String status;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	private String sourceid;
	
	private List<String> reasonCode=new ArrayList<String>();
	
	public List<String> getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(List<String> reasonCode) {
		this.reasonCode = reasonCode;
	}

	

	public String getSourceid() {
		return sourceid;
	}

	public void setSourceid(String sourceid) {
		this.sourceid = sourceid;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin(String isin) {
		this.isin = isin;
	}

	public String getCfi() {
		return cfi;
	}

	public void setCfi(String cfi) {
		this.cfi = cfi;
	}

	public String getParty() {
		return party;
	}

	public void setParty(String party) {
		this.party = party;
	}

	public String getTrading() {
		return trading;
	}

	public void setTrading(String trading) {
		this.trading = trading;
	}

	@Override
	public String toString() {
		return "Entity [id=" + id + ", sourceid=" + sourceid + ", isin=" + isin + ", cfi=" + cfi + ", reasonCode="
				+ reasonCode + ", party=" + party + ", trading=" + trading + "]";
	}

}
