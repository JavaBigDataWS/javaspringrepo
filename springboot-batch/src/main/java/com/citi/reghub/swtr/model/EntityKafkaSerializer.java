package com.citi.reghub.swtr.model;

import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.ByteBufferOutput;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.CompatibleFieldSerializer;
import com.esotericsoftware.kryo.serializers.FieldSerializer;

public class EntityKafkaSerializer implements Serializer<Entity>, Deserializer<Entity> {

	@SuppressWarnings("rawtypes")
	protected ThreadLocal kryos = new ThreadLocal() {

		@Override
		protected Object initialValue() {

			Kryo kryo = new Kryo();
			kryo.getFieldSerializerConfig()
					.setCachedFieldNameStrategy(FieldSerializer.CachedFieldNameStrategy.EXTENDED);
			CompatibleFieldSerializer serialzer = new CompatibleFieldSerializer<>(kryo, Entity.class);
			kryo.register(Entity.class, serialzer);
			return kryo;

		};
	};

	@Override
	public byte[] serialize(String topic, Entity entity) {
		try {

			int intSize = 1024;
			Output output = new ByteBufferOutput(intSize, intSize * 64);
			((Kryo) kryos.get()).writeObject(output, entity);
			output.flush();
			output.close();
			return output.toBytes();

		} catch (Exception e) {
			throw new RuntimeException("Unable to serialize");
		}

	}

	@Override
	public void configure(Map<String, ?> configs, boolean isKey) {
		// TODO Auto-generated method stub
		Deserializer.super.configure(configs, isKey);
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		Deserializer.super.close();
	}

	@Override
	public Entity deserialize(String topic, byte[] bytes) {
		
		try {
			Input input = new Input(bytes);
			Entity entity = ((Kryo) kryos.get()).readObject(input, Entity.class);
			input.close();
			return entity;
		}  catch (Exception e) {
			throw new RuntimeException("Unable to deserialize");
		}
		
	}

}
