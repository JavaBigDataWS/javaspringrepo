package com.citi.reghub.swtr.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.citi.reghub.swtr.model.Entity;

@Repository
public interface SwtrEntityRepository extends MongoRepository<Entity,Long> {

    
}