package com.citi.reghub.swtr.listener;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.stereotype.Component;

@Component
public class CustomJobListener implements JobExecutionListener {
 
    public void beforeJob(JobExecution jobExecution) {
        System.out.println("Called beforeJob().");
    }
 
    public void afterJob(JobExecution jobExecution) {
        System.out.println("Called afterJob().");
    }
}