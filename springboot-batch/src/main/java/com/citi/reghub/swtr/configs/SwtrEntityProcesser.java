package com.citi.reghub.swtr.configs;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.reghub.swtr.exception.NonReportbleException;
import com.citi.reghub.swtr.model.Entity;
import com.citi.reghub.swtr.repository.SwtrEntityRepository;

@Component
@StepScope
public class SwtrEntityProcesser implements ItemProcessor<Entity, Entity> {

	@Autowired
	private javax.validation.Validator validator;
	
	@Autowired
	private  SwtrEntityRepository swtrEntityRepository;

	@Override
	public Entity process(Entity item) throws Exception {

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
		Set<ConstraintViolation<Entity>> constraintViolations = validator.validate(item);
		item.getReasonCode().addAll(buildValidationException(constraintViolations));
		
		if(item.getReasonCode()!=null && item.getReasonCode().size()>0) {
			item.setStatus(SwtrConstant.NONREPORTABLE);
			swtrEntityRepository.save((Entity)item);
			throw new NonReportbleException();
		}
		else {
			item.setStatus(SwtrConstant.REPORTABLE);
			swtrEntityRepository.save((Entity)item);
			return item;	
		}
		
	}

	private List<String> buildValidationException(Set<ConstraintViolation<Entity>> constraintViolations) {
		List<String> msg = new ArrayList<String>();
		for (ConstraintViolation<Entity> error : constraintViolations) {
			msg.add(error.getMessage());
		}
		return msg;
	}

}
