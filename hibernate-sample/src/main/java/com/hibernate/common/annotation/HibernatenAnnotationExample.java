package com.hibernate.common.annotation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Transient;

import org.hibernate.Session;

import com.hibernate.common.util.HibernateUtil;

public class HibernatenAnnotationExample {

	public static void main(String[] args) {
		
		//elementCollection();
		//save();
		//immutable();
		//Transient();
		//temporal();
		//order();
		 natural();
	}
	
	private static void  natural() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		StudentA s1=new StudentA(1,"Atul",1);
		s1.setVehicleRegNum("PAN111");
		session.persist(s1);
		StudentA s=session.bySimpleNaturalId( StudentA.class ).load( "PAN111" );
    	System.out.println(s.getVehicleRegNum());
       
    		    	
		
	}

	private static void order() {
		Session session = HibernateUtil.getSessionFactory().openSession();
    	session.beginTransaction();
    	
    	State s1= new State(1,1,"UP");
    	State s2= new State(2,1,"MP");
    	State s3= new State(3,1,"HP");
    	State s4= new State(4,1,"DELHI");
    	
    	List<State> states= new ArrayList<State>();
    	states.add(s1);
    	states.add(s2);
    	states.add(s3);
    	states.add(s4);
    	
    	Country country= new Country(1,"India",states);
    	session.persist(country);
    	session.getTransaction().commit();
    	session.close();
		
	}

	private static void temporal() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Person p= new Person("naresh", "rai");
		StudentAnnotation s= new StudentAnnotation(p, "varanasi");
		StudentAnnotation s1= new StudentAnnotation(p, "varanasi");
		s.setCreateDate(Calendar.getInstance());
		s.setCity("Pune");
		session.save(s);
		//session.save(s1);
		session.getTransaction().commit();
		session.close();
		
	}

	private static void Transient() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Person p= new Person("naresh", "rai");
		StudentAnnotation s= new StudentAnnotation(p, "varanasi");
		StudentAnnotation s1= new StudentAnnotation(p, "varanasi");
		s.setCity("Pune");
		session.save(s);
		//session.save(s1);
		session.getTransaction().commit();
		session.close();
		
		
	}

	private static void immutable() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Person p= new Person("naresh", "rai");
		StudentAnnotation s= new StudentAnnotation(p, "varanasi");
		StudentAnnotation s1= new StudentAnnotation(p, "varanasi");
		session.save(s);
		//session.save(s1);
		session.getTransaction().commit();
		session.close();
		
		Session session2 = HibernateUtil.getSessionFactory().openSession();
		session2.beginTransaction();
		s.setLocation("xyz");
		session2.update(s);
		session2.getTransaction().commit();
		session2.refresh(s);
		System.out.println(s.getLocation());
	}

	private static void save() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Person p= new Person("naresh", "rai");
		StudentAnnotation s= new StudentAnnotation(p, "varanasi");
		StudentAnnotation s1= new StudentAnnotation(p, "varanasi");
		session.save(s);
		//session.save(s1);
		session.getTransaction().commit();
		session.close();
		
	}
	
	
	private static void elementCollection() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		College c=new College(1,"S.S.P.C");
    	session.persist(c);
        StudentA s1=new StudentA(1,"Atul",1);
        session.persist(s1);
        StudentA s2=new StudentA(2,"Saurabh",1);
        session.persist(s2);
    		    	
       	session.getTransaction().commit();
       	session.refresh(c);
       	College ob=(College)session.get(College.class, new Integer(1));
       	Set<String> names=ob.getStudents();
       	for(String s:names){
       		System.out.println(s);	
       	}
		
	}
	
}

