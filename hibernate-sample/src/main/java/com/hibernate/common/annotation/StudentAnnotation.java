package com.hibernate.common.annotation;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "StudentAnnotation")
@Immutable
public class StudentAnnotation implements Serializable {
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	@AttributeOverride(name = "firstName", column = @Column(name = "f_name"))
	private Person id;

	
	@Column(name = "location")
	private String location;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date")
	private Calendar createDate;

	
	public Calendar getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Calendar createDate) {
		this.createDate = createDate;
	}

	@Transient
	@Column(name = "city")
	private String city;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public StudentAnnotation(Person id, String location) {
		this.id = id;
		this.location = location;
	}

	public Person getId() {
		return id;
	}

	public void setId(Person id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}