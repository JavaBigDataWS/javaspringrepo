package com.hibernate.common.generator;

import org.hibernate.Session;

import com.hibernate.common.util.HibernateUtil;

public class HibernateIdGenExample {

	public static void main(String[] args) {
		
		save();
	}
	
	private static void save() {
		Session sessionOne = HibernateUtil.getSessionFactory().openSession();
		sessionOne.beginTransaction();

		EmployeeIdEntity emp = new EmployeeIdEntity();
		emp.setFirstName("Lokesh");
		emp.setLastName("Gupta");
		
		EmployeeIdEntity emp1 = new EmployeeIdEntity();
		emp1.setFirstName("Lokesh");
		emp1.setLastName("Gupta");

		sessionOne.persist(emp);
		sessionOne.persist(emp1);

		sessionOne.getTransaction().commit();
		
	}

	
}

