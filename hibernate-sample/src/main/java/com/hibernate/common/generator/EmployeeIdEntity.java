package com.hibernate.common.generator;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Employee_Generator")
public class EmployeeIdEntity implements Serializable {
	private static final long serialVersionUID = -1798070786993154676L;
	@Id
	/*@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="emp_seq")
	@SequenceGenerator(
	    name="emp_seq",
	    sequenceName="emp_seq",
	    allocationSize=1
	)*/
	
	
	/*@GeneratedValue(strategy=GenerationType.TABLE, generator="emp")
	@TableGenerator(
	    name="emp",
	    table="emp_id",
	    pkColumnName = "key",
	    valueColumnName = "next",
	    pkColumnValue="id",
	    allocationSize=20
	)
	
	
	
	@GeneratedValue(strategy=GenerationType.AUTO)
	*/
	
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
	private Integer employeeId;

	
	
	
	
	
	@Column(name = "FIRST_NAME", unique = false, nullable = false, length = 100)
	private String firstName;

	@Column(name = "LAST_NAME", unique = false, nullable = false, length = 100)
	private String lastName;

	
	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public int hashCode() {
		int result = getEmployeeId() != null ? getEmployeeId().hashCode() : 0;
		result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
		result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
		return result;
	}

	// Getters and Setters are hidden here
}