package com.hibernate.common.method;

import org.hibernate.Session;

import com.hibernate.common.util.HibernateUtil;

public class HibernateMethodExample {

	public static void main(String[] args) {
		
		persist();
		/*
		 * persist(); save(); merge();refresh(); load(); saveMethod(); saveorUpdate();
		*/
	}

	private static void merge() {
		Session sessionOne = HibernateUtil.getSessionFactory().openSession();
		sessionOne.beginTransaction();

		// Create new Employee object
		EmployeeMethodEntity emp = new EmployeeMethodEntity();
		emp.setEmployeeId(1);
		emp.setFirstName("Lokesh");
		emp.setLastName("Gupta");

		// Save employee
		sessionOne.save(emp);
		sessionOne.getTransaction().commit();
		sessionOne.close();

		// Verify employee's firstname
		System.out.println(verifyEmployeeFirstName(1, "Lokesh"));

		Session sessionTwo = HibernateUtil.getSessionFactory().openSession();
		sessionTwo.beginTransaction();

		// Set new first name
		emp.setFirstName("Vikas");

		// Merge the emp object using merge() method
		EmployeeMethodEntity mergedPersistentEmpEntity = (EmployeeMethodEntity) sessionTwo.merge(emp);

		sessionTwo.getTransaction().commit();
		sessionTwo.close();

		// Verify employee's firstname again in database
		System.out.println(verifyEmployeeFirstName(1, "Vikas"));

		HibernateUtil.shutdown();

	}

	private static void refresh() {
		Session sessionOne = HibernateUtil.getSessionFactory().openSession();
		sessionOne.beginTransaction();

		// Create new Employee object
		EmployeeMethodEntity emp = new EmployeeMethodEntity();
		emp.setEmployeeId(1);
		emp.setFirstName("Lokesh");
		emp.setLastName("Gupta");

		// Save employee
		sessionOne.save(emp);
		sessionOne.getTransaction().commit();
		sessionOne.close();

		// Verify employee's firstname
		System.out.println(verifyEmployeeFirstName(1, "Lokesh"));

		Session sessionTwo = HibernateUtil.getSessionFactory().openSession();
		sessionTwo.beginTransaction();

		// This
		emp.setFirstName("Vikas");
		sessionTwo.refresh(emp);

		sessionTwo.getTransaction().commit();
		sessionTwo.close();

		System.out.println(emp.getFirstName().equals("Lokesh"));

		HibernateUtil.shutdown();

	}

	private static void save() {
		Session sessionOne = HibernateUtil.getSessionFactory().openSession();
		sessionOne.beginTransaction();

		EmployeeMethodEntity emp = new EmployeeMethodEntity();
		emp.setEmployeeId(1);
		emp.setFirstName("Lokesh");
		emp.setLastName("Gupta");

		sessionOne.save(emp);
		System.out.println("Save Id" +sessionOne.save(emp));
		sessionOne.getTransaction().commit();
	}
	
	private static void persist() {
		Session sessionOne = HibernateUtil.getSessionFactory().openSession();
		sessionOne.beginTransaction();

		EmployeeMethodEntity emp = new EmployeeMethodEntity();
		emp.setFirstName("Lokesh");
		emp.setLastName("Gupta");

		sessionOne.persist(emp);

		sessionOne.getTransaction().commit();
	}

	public static void saveorUpdate() {
		Session sessionOne = HibernateUtil.getSessionFactory().openSession();
		sessionOne.beginTransaction();

		// Create new Employee object
		EmployeeMethodEntity emp = new EmployeeMethodEntity();
		emp.setEmployeeId(1);
		emp.setFirstName("Lokesh");
		emp.setLastName("Gupta");

		sessionOne.save(emp);
		sessionOne.getTransaction().commit();

		Session sessionTwo = HibernateUtil.getSessionFactory().openSession();
		sessionTwo.beginTransaction();

		emp.setLastName("temp");
		sessionTwo.save(emp);

		sessionTwo.getTransaction().commit();
	}

	public static void load() {
		Session sessionOne = HibernateUtil.getSessionFactory().openSession();
		sessionOne.beginTransaction();

		EmployeeMethodEntity emp = new EmployeeMethodEntity();
		emp.setFirstName("Lokesh");
		emp.setLastName("Gupta");

		sessionOne.save(emp);
		Integer empId = emp.getEmployeeId();
		sessionOne.getTransaction().commit();

		Session sessionTwo = HibernateUtil.getSessionFactory().openSession();
		sessionTwo.beginTransaction();

		EmployeeMethodEntity emp1 = (EmployeeMethodEntity) sessionTwo.load(EmployeeMethodEntity.class, empId);
		System.out.println(emp1.getFirstName() + " - " + emp1.getLastName());

		// Let's verify the entity name
		System.out.println(sessionTwo.getEntityName(emp1));

		sessionTwo.getTransaction().commit();

		/************************************************************************/

		Session sessionThree = HibernateUtil.getSessionFactory().openSession();
		sessionThree.beginTransaction();

		// second load() method example
		EmployeeMethodEntity emp2 = (EmployeeMethodEntity) sessionThree.load("com.howtodoinjava.demo.entity.EmployeeMethodEntity", empId);
		System.out.println(emp2.getFirstName() + " - " + emp2.getLastName());

		sessionThree.getTransaction().commit();

		/************************************************************************/

		Session sessionFour = HibernateUtil.getSessionFactory().openSession();
		sessionFour.beginTransaction();

		// third load() method example
		EmployeeMethodEntity emp3 = new EmployeeMethodEntity();
		sessionFour.load(emp3, empId);
		System.out.println(emp3.getFirstName() + " - " + emp3.getLastName());

		sessionFour.getTransaction().commit();

		HibernateUtil.shutdown();
	}

	private static boolean verifyEmployeeFirstName(Integer employeeId, String firstName) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		EmployeeMethodEntity employee = (EmployeeMethodEntity) session.load(EmployeeMethodEntity.class, employeeId);
		// Verify first name
		boolean result = firstName.equals(employee.getFirstName());
		session.close();
		// Return verification result
		return result;
	}
}