package com.hibernate.common.util;

import java.util.Properties;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import com.hibernate.common.dao.Student;

public class HibernateUtil {

	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {

		if (sessionFactory == null) {
			try {
				Configuration configuration = new Configuration();
				Properties settings = new Properties();
				settings.put(Environment.DRIVER, "oracle.jdbc.driver.OracleDriver");
				settings.put(Environment.URL, "jdbc:oracle:thin:@localhost:1521:xe");
				settings.put(Environment.USER, "system");
				
				/*settings.put("hibernate.cache.use_second_level_cache", "true");
				settings.put("hibernate.cache.region.factory_class","org.hibernate.cache.ehcache.EhCacheRegionFactory");
				settings.put("hibernate.cache.provider_class","net.sf.ehcache.hibernate.EhCacheProvider");*/

				
				settings.put(Environment.PASS, "admin");
				settings.put(Environment.DIALECT, "org.hibernate.dialect.OracleDialect");
				settings.put(Environment.SHOW_SQL, "true");
				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
				settings.put(Environment.HBM2DDL_AUTO, "create-drop");
				configuration.setProperties(settings);
				configuration.addAnnotatedClass(Student.class);
				configuration.addAnnotatedClass(com.hibernate.common.generator.EmployeeIdEntity.class);
				configuration.addAnnotatedClass(com.hibernate.common.method.EmployeeMethodEntity.class);
				configuration.addAnnotatedClass(com.hibernate.common.mapping.EmployeeMappingEntity.class);
				configuration.addAnnotatedClass(com.hibernate.common.cache.EmployeeCacheEntity.class);
				configuration.addAnnotatedClass(com.hibernate.common.mapping.AccountMappingEntity.class);
				configuration.addAnnotatedClass(com.hibernate.common.annotation.Person.class);
				configuration.addAnnotatedClass(com.hibernate.common.annotation.StudentAnnotation.class);
				configuration.addAnnotatedClass(com.hibernate.common.annotation.College.class);
				configuration.addAnnotatedClass(com.hibernate.common.annotation.StudentA.class);
				configuration.addAnnotatedClass(com.hibernate.common.annotation.State.class);
				configuration.addAnnotatedClass(com.hibernate.common.annotation.Country.class);
				
				
				
				
				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}

	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}
}