package com.hibernate.common.mapping;

import java.io.Serializable;
import java.util.Set;
 
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
 
@Entity(name = "ForeignKeyAssoEntity")
@Table(name = "EmployeeMapping")
public class EmployeeMappingEntity implements Serializable {
 
    private static final long serialVersionUID = -1798070786993154676L;
 
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="emp_seq")
	@SequenceGenerator(
	    name="emp_seq",
	    sequenceName="emp_seq",
	    allocationSize=1
	)
    private Integer employeeId;
 
    @Column(name = "EMAIL", unique = true, nullable = false, length = 100)
    private String email;
 
    @Column(name = "FIRST_NAME", unique = false, nullable = false, length = 100)
    private String firstName;
 
    @Column(name = "LAST_NAME", unique = false, nullable = false, length = 100)
    private String lastName;
 
    
    
    @OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="EMPLOYEE_ID")
    private Set<AccountMappingEntity> accounts;
    
    public Set<AccountMappingEntity> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<AccountMappingEntity> accounts) {
		this.accounts = accounts;
	}
	
    
   /* @OneToOne
    @JoinColumn(name="accountId")
    private AccountMappingEntity account;
    
    

	public AccountMappingEntity getAccount() {
		return account;
	}

	public void setAccount(AccountMappingEntity account) {
		this.account = account;
	}*/

	

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
 
    
}