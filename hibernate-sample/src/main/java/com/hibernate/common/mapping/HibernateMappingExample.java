package com.hibernate.common.mapping;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;

import com.hibernate.common.util.HibernateUtil;

public class HibernateMappingExample {

	public static void main(String[] args) {
		
		OnetoMany();
		OnetoOne();
	}

	private static void OnetoOne() {/*
		Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
 
        AccountMappingEntity account = new AccountMappingEntity();
        account.setAccountNumber("123-345-65454");
 
        // Add new Employee object
        EmployeeMappingEntity emp = new EmployeeMappingEntity();
        emp.setEmail("demo-user@mail.com");
        emp.setFirstName("demo");
        emp.setLastName("user");
 
        // Save Account
        session.saveOrUpdate(account);
        // Save Employee
        emp.setAccount(account);
        session.saveOrUpdate(emp);
 
        session.getTransaction().commit();
       
		
	*/}

	private static void OnetoMany() {
		
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	 
	        AccountMappingEntity account1 = new AccountMappingEntity();
	        account1.setAccountNumber("Account detail 1");
	 
	        AccountMappingEntity account2 = new AccountMappingEntity();
	        account2.setAccountNumber("Account detail 2");
	 
	        AccountMappingEntity account3 = new AccountMappingEntity();
	        account3.setAccountNumber("Account detail 3");
	 
	        //Add new Employee object
	        EmployeeMappingEntity firstEmployee = new EmployeeMappingEntity();
	        firstEmployee.setEmail("demo-user-first@mail.com");
	        firstEmployee.setFirstName("demo-one");
	        firstEmployee.setLastName("user-one");
	 
	        EmployeeMappingEntity secondEmployee = new EmployeeMappingEntity();
	        secondEmployee.setEmail("demo-user-second@mail.com");
	        secondEmployee.setFirstName("demo-two");
	        secondEmployee.setLastName("user-two");
	 
	        Set<AccountMappingEntity> accountsOfFirstEmployee = new HashSet<AccountMappingEntity>();
	        accountsOfFirstEmployee.add(account1);
	        accountsOfFirstEmployee.add(account2);
	 
	        Set<AccountMappingEntity> accountsOfSecondEmployee = new HashSet<AccountMappingEntity>();
	        accountsOfSecondEmployee.add(account3);
	 
	        firstEmployee.setAccounts(accountsOfFirstEmployee);
	        secondEmployee.setAccounts(accountsOfSecondEmployee);
	        //Save Employee
	        session.save(firstEmployee);
	        session.save(secondEmployee);
	 
	        session.getTransaction().commit();
	       
	    }
	}
	
