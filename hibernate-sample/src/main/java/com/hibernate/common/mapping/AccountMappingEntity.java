package com.hibernate.common.mapping;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "AccountMapping")
public class AccountMappingEntity implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="emp_seq")
	@SequenceGenerator(
	    name="emp_seq",
	    sequenceName="emp_seq",
	    allocationSize=1
	)
    private Integer           accountId;
    
    @Column(name = "ACC_NO", unique = false, nullable = false, length = 100)
    private String            accountNumber;
    
    
    
    @ManyToOne
    //@OneToOne(mappedBy="account")
    private EmployeeMappingEntity employee;
 
	public Integer getAccountId() {
		return accountId;
	}

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	
 
}