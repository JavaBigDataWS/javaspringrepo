package com.hibernate.common.cache;

import org.hibernate.Session;

import com.hibernate.common.util.HibernateUtil;

public class HibernateIdGenExample {

	public static void main(String[] args) {
		
		save();
	}
	
	private static void save() {
		Session sessionOne = HibernateUtil.getSessionFactory().openSession();
		sessionOne.beginTransaction();

		EmployeeCacheEntity emp = new EmployeeCacheEntity();
		emp.setEmployeeId(1111);
		emp.setFirstName("Lokesh");
		emp.setLastName("Gupta");
		

		sessionOne.save(emp);
		

		sessionOne.getTransaction().commit();
		sessionOne.close();
		
		Session two = HibernateUtil.getSessionFactory().openSession();
		two.beginTransaction();
		
		EmployeeCacheEntity  a=two.load(EmployeeCacheEntity.class, 1111);
		System.out.println(a.getFirstName()+"**************");
		two.close();
		
		Session two1 = HibernateUtil.getSessionFactory().openSession();
		two1.beginTransaction();
		
		EmployeeCacheEntity  b=two1.load(EmployeeCacheEntity.class, 1111);
		System.out.println(b.getFirstName()+"**************");
		two1.close();
		
	}

	
}

