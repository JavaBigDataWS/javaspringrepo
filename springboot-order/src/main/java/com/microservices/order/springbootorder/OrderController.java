package com.microservices.order.springbootorder;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import brave.sampler.Sampler;

@RestController
public class OrderController {
	
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	
	@GetMapping("/orderList")
	public ResponseEntity<List<String>> getBookList() {
		LOG.info("Inside order");
		return ResponseEntity.ok(OrderService.getBookList());
	}
	
	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}
	
	@GetMapping("/fallbackorder")
	public String test() {
		return "Hello You are in Fallback Service";
	}
	
	
	  
}