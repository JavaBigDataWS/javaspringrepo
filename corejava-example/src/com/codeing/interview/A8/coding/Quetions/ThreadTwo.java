package com.codeing.interview.A8.coding.Quetions;

public class ThreadTwo extends Thread{
	
	DeadlockExample deadlockexample=null;
	
	ThreadTwo(DeadlockExample deadlockexample){
		this.deadlockexample=deadlockexample;
	}
	
	@Override
	public void run() {
		deadlockexample.method2();
	}
}
