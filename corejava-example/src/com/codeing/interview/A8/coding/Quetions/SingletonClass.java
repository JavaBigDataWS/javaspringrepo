package com.codeing.interview.A8.coding.Quetions;

public class SingletonClass {

	private static SingletonClass singletonclass = null;

	private SingletonClass() {

	}

	public static SingletonClass getSingleton() {

		if (singletonclass == null) {
			singletonclass = new SingletonClass();
		}
		return singletonclass;
	}

	public static void main(String[] args) {
		System.out.println(SingletonClass.getSingleton());
		System.out.println(SingletonClass.getSingleton());
		System.out.println(SingletonClass.getSingleton());

	}

}
