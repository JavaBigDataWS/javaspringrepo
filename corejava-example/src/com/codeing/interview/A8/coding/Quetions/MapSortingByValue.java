package com.codeing.interview.A8.coding.Quetions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MapSortingByValue {

	public static void main(String[] args) {

		HashMap<String, Integer> hm = new HashMap<String, Integer>();
		hm.put("Arun5", 5);
		hm.put("Arun4", 4);
		hm.put("Arun3", 3);
		hm.put("Arun2", 2);
		hm.put("Arun1", 1);

		Set<Map.Entry<String, Integer>> set = hm.entrySet();

		List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(set);

		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {

			@Override
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {

				return o1.getValue().compareTo(o2.getValue());
			}

		});
		
		for (Entry<String, Integer> entry : list) {
			System.out.println("Key"+entry.getKey()+"Value"+entry.getValue());
			
		}
	}

}
