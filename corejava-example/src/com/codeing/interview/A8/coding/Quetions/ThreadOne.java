package com.codeing.interview.A8.coding.Quetions;

public class ThreadOne extends Thread{
	
	DeadlockExample deadlockexample=null;
	
	ThreadOne(DeadlockExample deadlockexample){
		this.deadlockexample=deadlockexample;
	}
	
	@Override
	public void run() {
		deadlockexample.method1();
	}
}
