package com.codeing.interview.A8.coding.Quetions;

public class DeadlockExample  {

	String str = "Arun";
	String str1 = "Kumar";

	public void method1() {
		while (true) {
			synchronized (str) {
				System.out.println("Method1");
				synchronized (str1) {
					System.out.println("Method11");
				}
			}
		}
	}

	public void method2() {
		while (true) {
			synchronized (str1) {
				System.out.println("method2");
				synchronized (str) {
					System.out.println("method22");
				}
			}
		}

	}

	

	public static void main(String[] args) {
		DeadlockExample exam=new DeadlockExample();
		ThreadOne examp1 = new ThreadOne(exam);
		ThreadTwo examp2 = new ThreadTwo(exam);
		examp1.start();
		examp2.start();

	}

}
