package com.codeing.interview.A5multithreading.Quetions;

import java.util.concurrent.ThreadLocalRandom;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class JoinExample extends Thread{

	public JoinExample() {
	}
	
	public void run(){
		System.out.println("run method");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		JoinExample t1=new JoinExample();
		JoinExample t2=new JoinExample();
		JoinExample t3=new JoinExample();
		//t1.join();
		t1.start();
		System.out.println("After run");
		
		try {
			t1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		t2.start();
		t3.start();
		
	
		
	}

}
