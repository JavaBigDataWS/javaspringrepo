package com.codeing.interview.A5multithreading.Quetions;

public class Deadlock1 extends Thread{
	
	String one="One";
	String two="Two";
	
	public Deadlock1(String one, String two) {
		// TODO Auto-generated constructor stub
	}

	public void run() {

		synchronized(one){
				System.out.println("Outer Deadlock1");
			synchronized(two){
				System.out.println("Inner Deadlock1");
			}
		}

	}

}
