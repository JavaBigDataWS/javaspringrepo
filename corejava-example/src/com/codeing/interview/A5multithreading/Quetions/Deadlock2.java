package com.codeing.interview.A5multithreading.Quetions;

public class Deadlock2 extends Thread{
	
	String one="One";
	String two="Two";
	public Deadlock2(String one, String two) {
		this.one=one;
		this.two=two;
	}

	public void run() {

		synchronized(String.class){
				System.out.println("Outer Deadlock2");
			synchronized(Integer.class){
				System.out.println("Inner Deadlock2");
			}
		}

	}

}
