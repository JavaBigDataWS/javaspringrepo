package com.codeing.interview.A5multithreading.Quetions;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

public class DeadLockManager {

	public static void main(String[] args) {

		final String one = "One";
		final String two = "Two";
		Deadlock1 a = new Deadlock1(one, two);
		Deadlock2 b = new Deadlock2(one, two);
		a.start();
		b.start();
		ThreadMXBean mx = ManagementFactory.getThreadMXBean();
		long[] arry = mx.findDeadlockedThreads();
		if (arry.length>1) {
			for (int i = 0; i < arry.length; i++) {
				System.out.println(i);
			}
		}
	}
}
