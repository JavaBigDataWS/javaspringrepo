package com.codeing.interview.java8.Quetions;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaStream {

	public static void main(String[] args) {
		
		List<String> strings = Arrays.asList("abc", "", "abc", "efg", "abcd","", "jkl");
		
	    Stream<String>  str= strings.stream();
				
	    Stream<String>  strr=str.filter(string -> !string.isEmpty());
	    
	    List<String> list=strr.collect(Collectors.toList());
	    
	    System.out.println(list);
		System.out.println(strings.stream().map(string ->string+"Hello").distinct().collect(Collectors.toList()));
	    System.out.println(strings.stream().map(string ->string+"Hello").distinct().limit(2).collect(Collectors.toList()));
	    System.out.println(strings.stream().map(string ->string+"hello").distinct().sorted().collect(Collectors.toList()));
	    System.out.println(strings.stream().filter(string ->string.equals("abc")).distinct().collect(Collectors.toList()));
	    
	    
	    List numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);

	    IntSummaryStatistics stats = null;//numbers.stream().mapToInt((x) -> x).summaryStatistics();

	    System.out.println("Highest number in List : " + stats.getMax());
	    System.out.println("Lowest number in List : " + stats.getMin());
	    System.out.println("Sum of all numbers : " + stats.getSum());
	    System.out.println("Average of all numbers : " + stats.getAverage());
	    
	    
		
	}
}
