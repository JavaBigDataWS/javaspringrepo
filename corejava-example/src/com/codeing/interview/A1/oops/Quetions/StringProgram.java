package com.codeing.interview.A1.oops.Quetions;

public class StringProgram {
	
	private static void checkObject() {
		String s1=new String("Arun");
		String s2="Arun";
		String s3="Arun";
		String s4=new String("Arun");
		System.out.println(s1==s3);
		System.out.println(s1==s2);
		System.out.println(s2==s3);
		System.out.println(s1==s4);
		System.out.println(s1.intern());
	}

	
	public static void main(String[] args) {
		checkObject();
	}

	
}
