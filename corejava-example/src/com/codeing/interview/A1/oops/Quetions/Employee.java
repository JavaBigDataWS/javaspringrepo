package com.codeing.interview.A1.oops.Quetions;

import java.util.Map;

public class Employee implements Cloneable {

	private int id;

	private String name;

	private Map<String, String> props;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, String> getProps() {
		return props;
	}

	public void setProps(Map<String, String> p) {
		this.props = p;
	}

	/*
	 * @Override public Object clone() throws CloneNotSupportedException { return
	 * super.clone(); }
	 */ public static void main(String[] args) throws CloneNotSupportedException {
		Employee emp = new Employee();
		Employee clonedEmp = (Employee) emp.clone();
		System.out.println(clonedEmp);
		System.out.println(emp);
	}
}
