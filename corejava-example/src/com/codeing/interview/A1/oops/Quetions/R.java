package com.codeing.interview.A1.oops.Quetions;

public class R extends Q{
	R(){
		System.out.println("R()");
	}
	{
		System.out.println("IIB-R");
	}
	R(int i){
		this();
		System.out.println("R(int)");
	}

	public static void main(String[] args) {
		Q q=new Q();
		System.out.println("----");
		Q q2=new Q(30);
		System.out.println("----");
		R r=new R();
		System.out.println("----");
		R r1=new R(40);
		System.out.println("----");
	}
}
