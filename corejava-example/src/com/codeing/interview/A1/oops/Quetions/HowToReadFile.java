package com.codeing.interview.A1.oops.Quetions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class HowToReadFile {

	public static void main(String[] args) throws IOException {

		
		FileReader f=new FileReader("Arun.txt");
		BufferedReader br=new BufferedReader(f);
		String str;
		while (( str=br.readLine()) != null)
		{
			System.out.println(str);
		}
		
		FileWriter fw=new FileWriter("Arun1.txt");
		BufferedWriter bw=new BufferedWriter(fw);
		bw.write("I am Arun ");
		bw.close();
	}

}
