package com.codeing.interview.A1.oops.Quetions;

import java.util.ArrayList;
import java.util.List;

public class Generics extends B1 {
	
	public static void main(String[] args) {
		List<B1> list=new ArrayList<B1>();
		B11 obj=new B11();
		list.add(obj);
				
		List<B11> listb11=new ArrayList<B11>();
	/*	
		list=listb11;
		listb11=list;
		*/
		
		List<Object> listOfObject = new ArrayList<Object>();
		List<String> listOfString = new ArrayList<String>();
		List<Integer> listOfInteger = new ArrayList<Integer>();
		
		listOfObject.add("Arun");
		listOfString.add("Arun");
		
		
		List<String> listOfString1 = new ArrayList();
		listOfString1.add("abcd");
		//listOfString1.add(1234);
		
		
		
		List a=new ArrayList();
		a.add("Arun");
		List<?> listOfObject2 = new ArrayList<>();
	//	listOfObject2.add("Arun");
		listOfObject2=a;
		
		
	}

}
