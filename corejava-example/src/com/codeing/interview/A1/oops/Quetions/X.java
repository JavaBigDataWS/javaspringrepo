package com.codeing.interview.A1.oops.Quetions;

class X{
static{
System.out.println("X static block");
}
{
System.out.println("Instance block 1");
}
public X(){
System.out.println("X constructor()");
}
{
System.out.println("Instance block 2");
}
}
class Y extends X{
static{
System.out.println("Y static block");
}
public Y(){
System.out.println("Y constructor()");
}
{
System.out.println("Instance block 3");
}
{
System.out.println("Instance block 4");
}
}
