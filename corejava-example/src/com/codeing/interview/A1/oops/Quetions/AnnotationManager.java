package com.codeing.interview.A1.oops.Quetions;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;



public class AnnotationManager {

	public static void main(String[] args) throws NoSuchMethodException, SecurityException {
	
		AnnotationManager obj=new AnnotationManager();
		obj.myAnnotationTestMethod();
	}

	@MyAnnotation(key = 10, value = "Arun")
	public void myAnnotationTestMethod() throws NoSuchMethodException, SecurityException {
		Class cls=this.getClass();
		 @SuppressWarnings("unchecked")
		Method mth = cls.getMethod("myAnnotationTestMethod");
		 MyAnnotation myAnno=mth.getAnnotation(MyAnnotation.class);
	       System.out.println("key: "+myAnno.key());
           System.out.println("value: "+myAnno.value());
	}
	
	
	
	

}
