package com.codeing.interview.A1.oops.Quetions;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class User implements Externalizable{

	@Override
	public String toString() {
		return "User [name=" + name + ", fatherName=" + fatherName + ", motherName=" + motherName + ", sal=" + sal
				+ ", income=" + income + "]";
	}
	private String name;
	private String fatherName;
	private String motherName;
	private int sal;
	private long income;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public int getSal() {
		return sal;
	}
	public void setSal(int sal) {
		this.sal = sal;
	}
	public long getIncome() {
		return income;
	}
	public void setIncome(long income) {
		this.income = income;
	}
	
	
	
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeInt(sal);
		out.writeLong(income);
		out.writeObject(name);
		
	}
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		this.sal=in.readInt();
		this.income=in.readLong();
			
	}
}
