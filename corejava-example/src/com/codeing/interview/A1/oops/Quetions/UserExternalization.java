package com.codeing.interview.A1.oops.Quetions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class UserExternalization {
	
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		User u=new User();
		u.setFatherName("Ram");
		u.setName("Mohan");
		u.setSal(2000);
		u.setIncome(200000);
		u.setMotherName("Sita");
		
		FileOutputStream fs=new FileOutputStream("User.ser");
		ObjectOutputStream obj=new ObjectOutputStream(fs);
		obj.writeObject(u);
		obj.close();
		
		
		
		FileInputStream fi= new FileInputStream("User.ser");
		ObjectInputStream o=new ObjectInputStream(fi);
		User uu=(User)o.readObject();
		System.out.println(uu);
		o.close();
		
		
		
	}

}
