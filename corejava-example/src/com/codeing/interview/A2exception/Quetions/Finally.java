package com.codeing.interview.A2exception.Quetions;

public class Finally {

	public static int show(){

		
		try {
			System.out.println("try");
			return 1;
			
		} catch (Exception e) {
			System.out.println("catch");
			return 1;
		}
		
		finally{
			System.out.println("finally");
			return 1;
		}
		
		
	}
	
	
	
	public static void main(String[] args) {

		System.out.println(show());
	}

}
