package com.codeing.interview.A4.string.Quetions;

public class AutoBox_Casting {
	
	public static void main(String[] args) {
	
		byte b=10;
		int i=b;
		byte c=(byte) i; //AutoBox Automatically and Downcasting Explicitly
		
		int h=10;
		byte hh=(byte) h;
		
		System.out.println(hh);
		
		
		
		double d=100.100;
		int v=(int) d;
		
	}

}
