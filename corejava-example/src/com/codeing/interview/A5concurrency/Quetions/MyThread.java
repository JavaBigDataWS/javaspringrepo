package com.codeing.interview.A5concurrency.Quetions;

import java.util.concurrent.CountDownLatch;

public class MyThread extends Thread{
	
	
	CountDownLatch latch;
	public MyThread(CountDownLatch c) {
		this.latch=c;
	}

	@Override
	public void run(){
		
		try {
			System.out.println("waiting for latch");
			latch.countDown();
			System.out.println("Got Latch");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
