package com.codeing.interview.A5concurrency.Quetions;

public class ReentrantLockExampleManager {

	public static void main(String[] args) {
		
		ReentrantLockExample t1=new ReentrantLockExample();
		ReentrantLockExample t2=new ReentrantLockExample();
		ReentrantLockExample t3=new ReentrantLockExample();
		ReentrantLockExample t4=new ReentrantLockExample();
		ReentrantLockExample t5=new ReentrantLockExample();
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();

	}

}
