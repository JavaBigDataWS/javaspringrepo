package com.codeing.interview.A5concurrency.Quetions;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockExample extends Thread{
	
	final private Lock lock=new ReentrantLock();
	
	public void run(){
		
		lock.lock();
		for (int i = 0; i <10; i++) {
			System.out.println("Item "+i);
		}
		lock.unlock();
	}
}
