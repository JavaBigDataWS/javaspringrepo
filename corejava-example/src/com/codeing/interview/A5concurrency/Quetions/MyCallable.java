package com.codeing.interview.A5concurrency.Quetions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MyCallable implements Callable<String>{
	
	@Override
	public String call() throws Exception {
		Thread.sleep(1000);
		Thread.currentThread().setName("MyCallable");
		return Thread.currentThread().getName();
	}
	
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService e=Executors.newFixedThreadPool(10);
		
		List<Future<String>> list = new ArrayList<Future<String>>();
		
		Callable<String> callable = new MyCallable();
		
		Callable<String> ycallable = new YourCallable();
		
		for (int i = 0; i < 100; i++) {
			
			Future<String> future=e.submit(callable);
			System.out.println("Count "+i);
			Future<String> yfuture=e.submit(ycallable);
			
			list.add(future);
			list.add(yfuture);
			
		}
		
		System.out.println("The End");
		
		/*for (Future< String> ftr: list) {
			
			if(ftr.isDone()){		//Checking the Statuc
			   ftr.cancel(true);    //Cancelling
				System.out.println("Cancelling the Job of "+Thread.currentThread().getName());
			}else{
				System.out.println("Job Done !!!!!!!!!!!!!");
			}
			
			
			System.out.println(ftr.get());//Checking the status
		}*/
	}

	

}
