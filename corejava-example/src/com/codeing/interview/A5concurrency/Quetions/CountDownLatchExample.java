package com.codeing.interview.A5concurrency.Quetions;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchExample {
	
	public static void main(String[] args) {
		
		
		CountDownLatch  c=new CountDownLatch(4);
		MyThread t1=new MyThread(c);
		MyThread t2=new MyThread(c);
		MyThread t3=new MyThread(c);
		MyThread t4=new MyThread(c);
		
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		/*try {
			c.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	System.out.println("Latch");	
	}

}
