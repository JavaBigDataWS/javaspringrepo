package com.codeing.interview.A5concurrency.Quetions;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarriereExample {

	static class MyRunnbleTread implements Runnable {

		public CyclicBarrier barrier;

		public MyRunnbleTread(CyclicBarrier barrier) {
			this.barrier = barrier;
		}

		@Override
		public void run() {
			try {

				System.out.println(Thread.currentThread().getName() + " is waiting on barrier");

				barrier.await();

				System.out.println(Thread.currentThread().getName() + " has crossed the barrier");

			} catch (InterruptedException e) {

				e.printStackTrace();

			} catch (BrokenBarrierException e) {

				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) {

		final CyclicBarrier cb = new CyclicBarrier(3);

		Thread t1 = new Thread(new MyRunnbleTread(cb), "Thread 1");
		Thread t2 = new Thread(new MyRunnbleTread(cb), "Thread 2");
		Thread t3 = new Thread(new MyRunnbleTread(cb), "Thread 3");

		t1.start();
		t2.start();
		t3.start();

	}
}