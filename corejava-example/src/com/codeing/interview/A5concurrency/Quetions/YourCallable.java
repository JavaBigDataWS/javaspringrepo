package com.codeing.interview.A5concurrency.Quetions;

import java.util.concurrent.Callable;

public class YourCallable implements Callable<String>{
	
	@Override
	public String call() throws Exception {
		Thread.sleep(1000);
		Thread.currentThread().setName("YourCallable");
		return Thread.currentThread().getName();
	}
	
}