package com.codeing.interview.designPattern.Quetions;

import java.io.Serializable;

public class MySingleton implements Serializable{

	private static MySingleton mysingleton = null;

	private MySingleton() {

	}

	public static MySingleton getSingletonObject() {

		if (mysingleton == null) {

			synchronized (MySingleton.class) {
				if (mysingleton == null) {

					 mysingleton = new MySingleton();
				}
				
				return mysingleton;
		}
		} else {
			return mysingleton;
		}
	}
	
	
	protected Object readResolve() {
	    return getSingletonObject();
	}

}
