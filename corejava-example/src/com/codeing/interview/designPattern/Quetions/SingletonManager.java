package com.codeing.interview.designPattern.Quetions;

public class SingletonManager {
	
	public static void main(String[] args) {
		
		MySingleton mysingleton=MySingleton.getSingletonObject();
		MySingleton mysingleton1=MySingleton.getSingletonObject();
		System.out.println(mysingleton);
		System.out.println(mysingleton1);
	}
}