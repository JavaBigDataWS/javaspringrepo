package com.codeing.interview.A7.collection.Quetions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class HashMapSorting {

	public static void main(String[] args) {
			
		Employee e1=new Employee();
		e1.setAge(12);
		e1.setName("zolu");
		e1.setIncome(12.3f);
		
		Employee e2=new Employee();
		e2.setAge(1);
		e2.setName("Arun");
		e2.setIncome(1.0f);
		
		Employee e3=new Employee();
		e3.setAge(12);
		e3.setName("Moulu");
		e1.setIncome(12.1f);
		
		Map<Employee,Integer> map=new HashMap<Employee,Integer>();
		map.put(e1, 5);
		map.put(e2, 2);
		map.put(e3, 7);
		
		
		
		Set<Map.Entry<Employee, Integer>> e=map.entrySet();
		
		List<Map.Entry<Employee, Integer>> list=new ArrayList<Map.Entry<Employee, Integer>>(e);
		
		Collections.sort(list, new Comparator<Map.Entry<Employee, Integer>>() {

			@Override
			public int compare(Entry<Employee, Integer> obj,Entry<Employee, Integer> obj1) {
				
				return obj.getKey().getName().compareTo(obj1.getKey().getName());
			}

			
		
		
		});
		
		Iterator<Map.Entry<Employee, Integer>> itr=list.iterator();
		while(itr.hasNext()){
			Entry<Employee,Integer> en=itr.next();
			Employee key=en.getKey();
			System.out.println("Sorting using Entry "+key);
		}
		
		
		
		Map<Employee,Integer> m=new TreeMap<>(new NameComparator());
							m.putAll(map);
		System.out.println("Sorting using the TreeMap "+m);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	/*	Set<Employee> set=map.keySet();
		Iterator<Employee> itr=set.iterator();
		while(itr.hasNext()){
			Employee e=itr.next();
			System.out.println(e.getName());
		}
		*/
		
		
	}

}
