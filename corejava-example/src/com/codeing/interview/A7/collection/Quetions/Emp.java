package com.codeing.interview.A7.collection.Quetions;

import java.util.HashMap;
import java.util.TreeMap;

public class Emp {
	
	private String name;
	private int age;
	
	
	public Emp(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	private float income;
	public float getIncome() {
		return income;
	}
	public void setIncome(float income) {
		this.income = income;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public static void main(String[] args) {
		HashMap p=new HashMap();
		Emp w=new Emp("1",1);
		Emp w1=new Emp("2",1);
		p.put(w,1);
		p.put(w1,1);
		
		Integer i=(Integer) p.get(new Emp("1",1));
		
		System.out.println("Key"+i);
		
		/*TreeMap m=new TreeMap(p);
		System.out.println(m.size());*/
		
	}
	
}
