package com.codeing.interview.A7.collection.Quetions;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.WeakHashMap;

public class Tail_Head_Weak_HashMap_Example {
	
	public static void main(String[] args) {
		
		
		Employee e1=new Employee();
		e1.setAge(12);
		e1.setName("zolu");
		e1.setIncome(12.3f);
		
		Employee e2=new Employee();
		e2.setAge(1);
		e2.setName("Arun");
		e2.setIncome(1.0f);
		
		Employee e3=new Employee();
		e3.setAge(12);
		e3.setName("Moulu");
		e1.setIncome(12.1f);
		
		
		Map<Employee,Integer> map=new WeakHashMap<Employee,Integer>();
		map.put(e1, 5);
		map.put(e2, 2);
		map.put(e3, 7);
		
		
		System.out.println("***************WeakHashMap************");
		System.out.println("Before Nullfying WeakHashMap" +map);
		e1=null;
		System.gc();
		System.out.println("After Nullfying " +map);
		
		System.out.println("/n");
		
		Map<Employee,Integer> smap=new HashMap<Employee,Integer>();
		smap.put(e1, 5);
		smap.put(e2, 2);
		smap.put(e3, 7);
		System.out.println("***************NormalHashMap************");
		
		System.out.println("Before Nullfying NormalHashMap" +map);
		e1=null;
		System.gc();
		System.out.println("After Nullfying NormalHashMap" +map);
		System.out.println("/n");
		
		TreeMap<Employee,Integer> t=new TreeMap<Employee,Integer>();
								t.put(e1, 1);
								t.put(e2, 1);
								t.put(e3, 1);
		
		
		
	}

}
