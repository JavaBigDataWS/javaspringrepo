package com.codeing.interview.A7.collection.Quetions;

import java.util.Comparator;
import java.util.TreeSet;

public class Employee implements Comparable<Employee>{
	
	private String name;
	private int age;
	private float income;
	public float getIncome() {
		return income;
	}
	public void setIncome(float income) {
		this.income = income;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	@Override
	public int compareTo(Employee obj){
		//return name.compareTo(obj.getName());
		if(age > obj.getAge()) 
		{return 1;
		}
		else if (age==obj.getAge()) {
				int k=name.compareTo(obj.getName());
								 if(k==0){
											if(income>obj.getIncome()) return 1;
											else if(income==obj.getIncome()) return 0;
											else return -1;
								
								 }else{
									return 0;
								}
		}
		else return -1;
	}
	
	
	@Override
	public int hashCode(){
		return this.name.hashCode()+new Integer(this.age).hashCode();
	}
	
	@Override
	public boolean equals(Object obj){
			if (!(obj instanceof Employee)) {
				return false;
			}
			else{
				Employee e=(Employee) obj;
				int age=e.getAge();
				String name=e.getName();
				if(this.age==age && this.name.equals(name)){
					return true;
				}else {
					return false;
				}
			}
	}
	
	
	@Override
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + ", income=" + income + "]";
	}
	
	
	
	static class NameComparator implements Comparator<Employee>{
		
		
		@Override
		public int compare(Employee obj0,Employee obj){
			if(obj0.getAge() > obj.getAge()) 
			{return 1;
			}
			else if (obj0.getAge()==obj.getAge()) {return 0;}
			else return -1;
		}
	}
	
	
	public static void main(String[] args) {
		TreeSet<Employee> set=new TreeSet<Employee>();
		Employee e1=new Employee();
		e1.setAge(12);
		e1.setName("Golu");
		e1.setIncome(12.3f);
		
		Employee e2=new Employee();
		e2.setAge(1);
		e2.setName("Arun");
		e2.setIncome(1.0f);
		
		Employee e3=new Employee();
		e3.setAge(12);
		e3.setName("Golu");
		e1.setIncome(12.1f);
		
		set.add(e1);
		set.add(e2);
		set.add(e3);
		
		
		System.out.println(set);
		
		TreeSet t=new TreeSet(new NameComparator());
				t.addAll(set);
		System.out.println(t);
	}
}
