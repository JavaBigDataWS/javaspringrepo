package com.codeing.interview.A7.collection.Quetions;

import java.util.HashMap;

public class HashMapProgram {

	private static void occuranceOfChar() {
		String str = "abcabcabc";
		HashMap<String, Integer> hm = new HashMap<String, Integer>();
		for (int i = 0; i < str.length(); i++) {
			String key = str.substring(i, i + 1);
			if (hm.containsKey(key)) {
				int value = hm.get(key);
				hm.put(key, ++value);
			} else {
				hm.put(key, 1);
			}
		}
		System.out.println(hm);
	}
	
	
	public static void main(String[] args) {
		occuranceOfChar();
	}

	
}
