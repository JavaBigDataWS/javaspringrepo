package com.codeing.interview.A7.collection.Quetions;

import java.util.HashSet;
import java.util.TreeSet;

public class HashSetProgram {
	
	public static void main(String[] args) {
		TreeSet<Employee> set=new TreeSet<Employee>();
		Employee e1=new Employee();
		e1.setAge(12);
		e1.setName("Golu");
		e1.setIncome(12.3f);
		
		Employee e2=new Employee();
		e2.setAge(1);
		e2.setName("Arun");
		e2.setIncome(1.0f);
		
		Employee e3=new Employee();
		e3.setAge(12);
		e3.setName("Golu");
		e1.setIncome(12.1f);
		
		set.add(e1);
		set.add(e2);
		set.add(e3);
		
		
		System.out.println(set);
		
		TreeSet t=new TreeSet(set);
		
		
	}
	
	
}
