package com.codeing.interview.A7.collection.Quetions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ArrayListProgram {
	
	public static void main(String[] args) {
		
		List<Employee> list=new ArrayList<Employee>();
		
		
		List<Employee> list1=new ArrayList<Employee>();
		list1.add(null);
		System.out.println("List1 "+list1);
		
		
		Set<Employee> set1=new HashSet<Employee>();
		set1.add(null);
		System.out.println("set1 "+set1);
		
		
		
		
		Employee e1=new Employee();
		e1.setAge(12);
		e1.setName("Golu");
		e1.setIncome(12.3f);
		
		Employee e2=new Employee();
		e2.setAge(1);
		e2.setName("Arun");
		e2.setIncome(1.0f);
		
		Employee e3=new Employee();
		e3.setAge(12);
		e3.setName("Golu");
		e1.setIncome(12.1f);
		
		list.add(e1);
		list.add(e2);
		list.add(e3);
		
		Collections.sort(list);
		
		System.out.println(list);
	}

}
