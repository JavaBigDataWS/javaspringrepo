package com.codeing.interview.scjp.Quetions;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetManager {
	
	public static void main(String[] args) {
		Set<String> s=new HashSet<>();
			s.add("Arun0");
			s.add("Arun1");
			s.add("Arun2");
			s.add("Arun3");
			s.add("Arun4");
			
		Iterator<String> itr=s.iterator();
		
		while(itr.hasNext()){
			String str=itr.next();
			itr.remove();
			s.add(str);
		}
	}

}
