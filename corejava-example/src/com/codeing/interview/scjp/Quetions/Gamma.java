package com.codeing.interview.scjp.Quetions;

public class Gamma {

	static Foo foo;
	static Foo fooBar(Foo foo) {
		
		foo = new Foo(100);
		return foo;
	}

	public static void main(String[] args) {

		Foo foo = new Foo(300);

		Foo fooFoo = fooBar(foo);
		
		System.out.println(foo.getX());
		System.out.println(fooFoo.getX());//100
	}

}
