package com.codeing.interview.scjp.Quetions;

public class PassByValue {
	
	private int num;
	
	public void setNum(int num){
		this.num=num;
	}
	
	
	public int getNum(){
		return num;
	}
	public  PassByValue changeNum(PassByValue p){
		
		p=new PassByValue();
		p.setNum(200);
		return p;
	}
	
	public static void main(String[] args) {
		
		PassByValue obj=new PassByValue();
		obj.setNum(100);
		System.out.println(obj.getNum());
		obj.changeNum(obj);
		System.out.println(obj.getNum());
		
	}

}
