package com.codeing.interview.scjp.Quetions;

public class MyThread extends Thread {

	
	private int result;
	private int num;
	private boolean flag;
	
	public MyThread(int i) {
		this.num=num;
	}

	@Override
	public synchronized void run() {
		result=num*2;
		flag=true;
		System.out.println("result"+result);
		notify();
		
	}
	
	public synchronized int getResult() throws InterruptedException {
		System.out.println("getResult");
		
		while (!flag) {
			wait();
		}
		
		return result;
		
	}
	
	
	public static void main(String[] args) throws InterruptedException {

		MyThread[] t=new MyThread[4];
		
		for (int i = 0; i < t.length; i++) {
			t[i]=new MyThread(i);
			t[i].start();
		}
		
		
		for (MyThread myThread : t) {
			System.out.println(myThread.getResult()+"");
		}
	}

}
