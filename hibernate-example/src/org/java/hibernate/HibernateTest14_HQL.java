package org.java.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.java.dto.UserDetails11;


public class HibernateTest14_HQL {

	public static void main(String[] args) {
		
		
		Configuration con = new Configuration().configure();
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		
		/*Query q=session.createQuery("from UserDetails11");
		q.setFirstResult(5);
		q.setMaxResults(5);
		List<UserDetails11>	list= q.list();
		
		for (UserDetails11 userDetails11 : list) {
			System.out.println(userDetails11.getUserName());
			
		}*/
		
		
		/*Query q=session.createQuery("select userName from UserDetails11");
		q.setFirstResult(5);
		q.setMaxResults(5);
		List<String>	list= q.list();
		
		for (String userDetails11 : list) {
			System.out.println(userDetails11);
			
		}*/
		
		/*Query q=session.createQuery("select userName,UserId from UserDetails11");
		q.setFirstResult(5);
		q.setMaxResults(5);
		
		
		session.getTransaction().commit();
		session.close();*/
		
		
		Query q=session.createQuery("select max(UserId) from UserDetails11");
		q.setFirstResult(5);
		q.setMaxResults(5);
		
		
		session.getTransaction().commit();
		session.close();
		
		/*  Not able to Pagination 
		 * 
		 * 
		 */
		


	}

}
