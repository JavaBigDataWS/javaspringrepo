package org.java.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.java.dto.UserDetails11;


public class HibernateTest15_NAMED_QUERY {

	public static void main(String[] args) {
		
		
		Configuration con = new Configuration().configure();
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		
		/*Query i=session.getNamedQuery("UserDetails.byId");
		i.setInteger(0, 38);
		List<UserDetails11> li=i.list();
		for (UserDetails11 userDetails11 : li) {
			System.out.println(userDetails11.getUserName());
		}
		*/
		
		Query i=session.getNamedQuery("UserDetails.byName");
		i.setString(0, "Arun0");
		List<UserDetails11> li=i.list();
		for (UserDetails11 userDetails11 : li) {
			System.out.println(userDetails11.getUserName());
		}
		
		
		
		/*
		 * 
		 */
		
		session.getTransaction().commit();
		session.close();
		

	}

}
