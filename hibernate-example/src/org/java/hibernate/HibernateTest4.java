package org.java.hibernate;

import java.util.ArrayList;
import java.util.Collection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.java.dto.Address2;
import org.java.dto.Address4;
import org.java.dto.UserDetails4;


public class HibernateTest4 {

	public static void main(String[] args) {

		
		UserDetails4 o = new UserDetails4();
		o.setUserName("Arun");

		Address4 a=new  Address4();
		a.setCity("Banda");
		a.setPincode("111");
		a.setState("up");
		a.setStreet("Santoshi mata");
		
		Address4 a2=new  Address4();
		a2.setCity("homeBanda");
		a2.setPincode("home111");
		a2.setState("homeup");
		a2.setStreet("homeSantoshi mata");
		
		Collection<Address4> t=new ArrayList<Address4>(); t.add(a);t.add(a2);
		
		o.getListofAddress().addAll(t);
		
		Configuration con = new Configuration().configure();
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		int primaryKey=(Integer) session.save(o);
		System.out.println("Primary Key"+primaryKey);
		session.getTransaction().commit();
		session.close();
		
		session = sf.openSession();
		UserDetails4 u=(UserDetails4)session.get(UserDetails4.class, 1);
		session.close();
		System.out.println(u.getListofAddress().size());
		
		
	}

}
