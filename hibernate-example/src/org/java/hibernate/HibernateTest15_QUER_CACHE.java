package org.java.hibernate;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateTest15_QUER_CACHE {

	public static void main(String[] args) {

		Configuration con = new Configuration().configure();
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		session = sf.openSession();
		session.beginTransaction();

		Query q = session.createQuery("from UserDetails11");
		q.setCacheable(true);
		System.out.println(q.list().size());
		session.getTransaction().commit();
		session.close();

		session = sf.openSession();
		session.beginTransaction();

		Query q1 = session.createQuery("from UserDetails11");
		q1.setCacheable(true);
		System.out.println(q1.list().size());

		session.getTransaction().commit();
		session.close();

	}

}
