package org.java.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.java.dto.UserDetails11;


public class HibernateTest14_HQL_PARAMETER {

	public static void main(String[] args) {
		
		
		Configuration con = new Configuration().configure();
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		
		
		//Query q=session.createQuery("from UserDetails11 where userId > ?");
		
		Query q=session.createQuery("from UserDetails11 where userId > :userId");
		q.setInteger("userId", 35);
		//q.setInteger(0, 35);
		List<UserDetails11> list=q.list();
		for (UserDetails11 userDetails11 : list) {
			System.out.println(userDetails11.getUserName());
		}
		
		session.getTransaction().commit();
		session.close();
		

	}

}
