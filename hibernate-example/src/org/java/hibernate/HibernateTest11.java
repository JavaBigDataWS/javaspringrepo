package org.java.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.java.dto.UserDetails11;

public class HibernateTest11 {

	public static void main(String[] args) {

		Configuration con = new Configuration().configure();
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();

		for (int i = 0; i < 10; i++) {
			UserDetails11 o = new UserDetails11();
			o.setUserName("Arun" + i);
			session.save(o);
		}
		
		
		
		
		//UserDetails11 UserDetails11=(UserDetails11)session.get(UserDetails11.class,4);
		
		//System.out.println("UserId "+UserDetails11.getUserId()+"Name :"+UserDetails11.getUserName());

		//UserDetails11.setUserName("updated useer");
		//session.update(UserDetails11);
		//session.delete(UserDetails11);
		session.getTransaction().commit();
		session.close();
		
		//System.out.println("UserId "+UserDetails11.getUserId()+"Name :"+UserDetails11.getUserName());


	}

}
