package org.java.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.java.dto.UserDetails11;

public class HibernateTest13 {

	public static void main(String[] args) {
/*
		UserDetails11 o = new UserDetails11();
		o.setUserName("Arun")	;*/
		
		
		Configuration con = new Configuration().configure();
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		
		
		
		UserDetails11 o=(UserDetails11) session.get(UserDetails11.class, 1);
		
		/*o.setUserName("Updated UserName");
		o.setUserName("Updated UserName");*/
		
		//session.update(o);
		//session.update(o);  only one update is happen hibernate track the object
		
		
		
		/*
		if we are not passing the object to save then that ibject is transiate 
		object once save it become persistant object. once session.close it become 
		detanched and hibernate is not gonee to track 
		changes being done in it. 
		*/
		
		
		
		session.getTransaction().commit();
		session.close();
		
		
		o.setUserName("chnage");
		
		session = sf.openSession();
		session.beginTransaction();
		session.update(o);
		session.getTransaction().commit();
		session.close();
		
		
		//if you are closing the sessiona after that you aRE NOT updateing but stilll calling update then alos
		//it will update in database in this case we can use @org.hibernate.annotations.Entity(selectBeforeUpdate=true)
		


	}

}
