package org.java.hibernate;

import java.util.ArrayList;
import java.util.Collection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.java.dto.Address2;
import org.java.dto.UserDetails3;


public class HibernateTest3 {

	public static void main(String[] args) {

		
		UserDetails3 o = new UserDetails3();
		o.setUserName("Arun");
		
		
		
		Address2 a=new  Address2();
		a.setCity("Banda");
		a.setPincode("111");
		a.setState("up");
		a.setStreet("Santoshi mata");
		
		Address2 a2=new  Address2();
		a2.setCity("homeBanda");
		a2.setPincode("home111");
		a2.setState("homeup");
		a2.setStreet("homeSantoshi mata");
		
		Collection<Address2> t=new ArrayList<Address2>(); t.add(a);t.add(a2);
		
		o.getListofAddress().addAll(t);
		
		Configuration con = new Configuration().configure();
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		int primaryKey=(Integer) session.save(o);
		System.out.println("Primary Key"+primaryKey);
		session.getTransaction().commit();
		session.close();
		
	}

}
