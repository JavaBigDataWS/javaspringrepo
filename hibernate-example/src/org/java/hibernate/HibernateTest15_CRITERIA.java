package org.java.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.java.dto.UserDetails11;
import org.java.dto.UserDetails12;

public class HibernateTest15_CRITERIA {

	public static void main(String[] args) {
		
		
		Configuration con = new Configuration().configure();
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		
		/*Criteria c=session.createCriteria(UserDetails11.class).setProjection(Projections.property("userId"));
		//Criteria c=session.createCriteria(UserDetails11.class).setProjection(Projections.max("userId"));
	    //c.add(Restrictions.or(Restrictions.between("userName", "A", "B"),Restrictions.between("userId", 1, 5)));
		//c.add(Restrictions.eq("userName", "Arun0"));
		c.add(Restrictions.gt("userId", 30));
		
		List<UserDetails11>	list= c.list();
		System.out.println(list.size());
		
		for (UserDetails11 userDetails11 : list) {
		
			System.out.println("Name"+userDetails11.getUserName());
			System.out.println("Name"+userDetails11.getUserId());
		}*/
		
		
		/*Criteria c=session.createCriteria(UserDetails11.class).setProjection(Projections.property("userId"));
		c.add(Restrictions.gt("userId", 30));
		
		List<Integer>	list= c.list();
		System.out.println(list.size());
		
		for (Integer userDetails11 : list) {
		
			System.out.println("Name"+userDetails11);
		}
		
		*/
		
		
		/*Criteria c=session.createCriteria(UserDetails11.class).setProjection(Projections.max("userId"));
		c.add(Restrictions.gt("userId", 30));
		
		List<Integer>	list= c.list();
		System.out.println(list.size());
		
		for (Integer userDetails11 : list) {
		
			System.out.println("Name"+userDetails11);
		}
		*/
		
		
		/*Criteria c=session.createCriteria(UserDetails11.class).setProjection(Projections.count("userId"));
		c.add(Restrictions.gt("userId", 30));
		
		List<Long>	list= c.list();
		System.out.println(list.size());
		
		for (Long userDetails11 : list) {
		
			System.out.println("Name"+userDetails11);
		}*/
		
		
		
		
		
		/*Criteria c=session.createCriteria(UserDetails11.class).setProjection(Projections.property("userId")).addOrder(Order.desc("userId"));
		c.add(Restrictions.gt("userId", 30));
		
		List<Integer>	list= c.list();
		System.out.println(list.size());
		
		for (Integer userDetails11 : list) {
		
			System.out.println("Name"+userDetails11);
		}
		*/
		
		
		/*UserDetails11 n=new UserDetails11();
		n.setUserId(38);
		
		
		Example e=Example.create(n);*/
		//Example e=Example.create(n).excludeProperty("userName");
		//Example e=Example.create(n).excludeProperty("userName").enableLike();
		
		//Criteria c=session.createCriteria(UserDetails11.class).add(e);

		//Criteria c=session.createCriteria(UserDetails11.class).add(e);
		
		
		
		/*List<UserDetails11>	list= c.list();
		System.out.println(list.size());
		
		for (UserDetails11 userDetails11 : list) {
		
			//System.out.println("Name"+userDetails11.getUserId());
		}
		
		
		session.getTransaction().commit();
		session.close();
		*/
		
		session = sf.openSession();
		session.beginTransaction();
		session.get(UserDetails11.class, 31);
		session.getTransaction().commit();
		session.close();
		
		session = sf.openSession();
		session.beginTransaction();
		session.get(UserDetails11.class, 31);
		session.getTransaction().commit();
		session.close();

		
		
		/*
		 * IF you are setting null or primary key in the object then it wount consider means it will return whole object to you.
		 */
	}

}
