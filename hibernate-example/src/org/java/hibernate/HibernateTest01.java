package org.java.hibernate;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.java.dto.Address;
import org.java.dto.LoginName;
import org.java.dto.UserDetails;


public class HibernateTest01 {

	public static void main(String[] args) {

		UserDetails o = new UserDetails();
		//o.setUserId(180);
		
		//o.setUserLoginId(l);
		o.setUserName("Arun");
		o.setAddress("Adderess");
		o.setDescription("Description");
		o.setJoinDate(new Date());
		
		UserDetails o2 = new UserDetails();
		LoginName l=new LoginName(); l.setLastName("A");l.setName("jjjj");
		o2.setUserLoginId(l);
		o2.setUserName("Arun");
		o2.setAddress("Adderess");
		o2.setDescription("Description");
		o2.setJoinDate(new Date());
		
		Address a=new  Address();
		a.setCity("Banda");
		a.setPincode("111");
		a.setState("up");
		a.setStreet("Santoshi mata");
		
		Address ahome=new  Address();
		a.setCity("homeBanda");
		a.setPincode("home111");
		a.setState("homeup");
		a.setStreet("homeSantoshi mata");
		
		o2.setAddres(a);
		o2.setHomeAddress(ahome);
		
		
		
		Configuration con = new AnnotationConfiguration().configure();
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		session.save(o2);
		//System.out.println("Primary Key"+primaryKey);
		session.getTransaction().commit();
		session.close();
		
		
		
		
		/*sf = con.buildSessionFactory();
		session = sf.openSession();
		o=(UserDetails) session.get(UserDetails.class, 180);
		System.out.println(o);*/
		
	}

}
