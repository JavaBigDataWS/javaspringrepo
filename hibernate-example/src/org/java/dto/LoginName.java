package org.java.dto;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class LoginName implements Serializable{
	
	private String name;
	private String lastName;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	

}
