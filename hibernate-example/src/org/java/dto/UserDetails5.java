package org.java.dto;

import static javax.persistence.GenerationType.AUTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CollectionId;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "USER_DETAILS5")
public class UserDetails5 {

	@Id @GeneratedValue(strategy=AUTO)
	private int userId;
	
	@Column(name="USER_NAME")
	private String userName;
	
	@OneToOne
	@JoinColumn(name="VEHICAL_ID")
	private Vehical Vehical;
	

	
	public Vehical getVehical() {
		return Vehical;
	}

	public void setVehical(Vehical vehical) {
		Vehical = vehical;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
		
	
	
	
}