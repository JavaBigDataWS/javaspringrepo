package org.java.dto;

import static javax.persistence.GenerationType.AUTO;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@NamedQuery(name="UserDetails.byId",query="from UserDetails11 where userId > ? ")
@NamedNativeQuery(name="UserDetails.byName",query="select * from USER_DETAILS11 where USER_NAME = ?",resultClass=UserDetails11.class)
@org.hibernate.annotations.Entity(selectBeforeUpdate=true)
@Table(name = "USER_DETAILS11")
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class UserDetails11 {

	@Id @GeneratedValue(strategy=AUTO)
	private int userId;
	
	@Column(name="USER_NAME")
	private String userName;
	
	
	public int getUserId() {
		return userId;
	}

	

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}



	@Override
	public String toString() {
		return "UserDetails11 [userId=" + userId + ", userName=" + userName + "]";
	}

	
		
	
	
	
}