package org.java.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="VEHICAL7")
public class Vehical7 {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private int vehicalId;
	
	private String vehicalName;
	
	
	@ManyToOne
	private UserDetails7 userdetails7;  //optional
	
	public UserDetails7 getUserdetails7() {
		return userdetails7;
	}
	public void setUserdetails7(UserDetails7 userdetails7) {
		this.userdetails7 = userdetails7;
	}
	public int getVehicalId() {
		return vehicalId;
	}
	public void setVehicalId(int vehicalId) {
		this.vehicalId = vehicalId;
	}
	public String getVehicalName() {
		return vehicalName;
	}
	public void setVehicalName(String vehicalName) {
		this.vehicalName = vehicalName;
	}
	
	
}
