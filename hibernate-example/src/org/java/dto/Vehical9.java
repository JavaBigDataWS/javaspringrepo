package org.java.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="VEHICAL9")
public class Vehical9 {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private int vehicalId;
	
	private String vehicalName;
	
	
	@ManyToOne
	@JoinColumn(name="USER_ID")
	private UserDetails9 userdetails9;  //optional
	
	public UserDetails9 getUserdetails9() {
		return userdetails9;
	}
	public void setUserdetails9(UserDetails9 userdetails9) {
		this.userdetails9 = userdetails9;
	}
	public int getVehicalId() {
		return vehicalId;
	}
	public void setVehicalId(int vehicalId) {
		this.vehicalId = vehicalId;
	}
	public String getVehicalName() {
		return vehicalName;
	}
	public void setVehicalName(String vehicalName) {
		this.vehicalName = vehicalName;
	}
	
	
}
