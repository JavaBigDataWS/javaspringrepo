package org.java.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="VEHICAL8")
public class Vehical8 {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private int vehicalId;
	
	private String vehicalName;
	
	
	@ManyToOne
	@JoinColumn(name="USER_ID")
	private UserDetails8 userdetails8;  //optional
	
	public UserDetails8 getUserdetails8() {
		return userdetails8;
	}
	public void setUserdetails8(UserDetails8 userdetails8) {
		this.userdetails8 = userdetails8;
	}
	public int getVehicalId() {
		return vehicalId;
	}
	public void setVehicalId(int vehicalId) {
		this.vehicalId = vehicalId;
	}
	public String getVehicalName() {
		return vehicalName;
	}
	public void setVehicalName(String vehicalName) {
		this.vehicalName = vehicalName;
	}
	
	
}
