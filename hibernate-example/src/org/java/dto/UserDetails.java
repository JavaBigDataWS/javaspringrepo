package org.java.dto;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "USER_DETAILS")
public class UserDetails {

	@Embedded
	private Address addres;

	
	public Address getAddres() {
		return addres;
	}

	public void setAddres(Address addres) {
		this.addres = addres;
	}
	
	
	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="street",column=@Column(name="STREET_HOME")),	
		@AttributeOverride(name="city",column=@Column(name="CITY_HOME"))	,
		@AttributeOverride(name="state",column=@Column(name="STATE_HOME"))	,
		@AttributeOverride(name="pincode",column=@Column(name="pincode_HOME"))	
		
	})
	
	private Address homeAddress;

	
	
	public Address getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(Address homeAddress) {
		this.homeAddress = homeAddress;
	}


	/*@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name = "USER_ID")
	private int userId;
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	
	*/
	
	
	@EmbeddedId
	private LoginName userLoginId;
	

	public LoginName getUserLoginId() {
		return userLoginId;
	}

	public void setUserLoginId(LoginName userLoginId) {
		this.userLoginId = userLoginId;
	}


	@Temporal(TemporalType.TIMESTAMP)
	private Date joinDate;

	@Transient
	private static String address;

	/* @Lob */
	private String description;

	@Column(name = "USER_NAME")
	private String userName;

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/*@Override
	public String toString() {
		return "UserDetails [userId=" + userId + ", joinDate=" + joinDate + ", description=" + description
				+ ", userName=" + userName + ", getJoinDate()=" + getJoinDate() + ", getAddress()=" + getAddress()
				+ ", getDescription()=" + getDescription() + ", getUserId()=" + getUserId() + ", getUserName()="
				+ getUserName() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}*/

}
