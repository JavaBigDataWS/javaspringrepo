package org.java.dto;

import static javax.persistence.GenerationType.AUTO;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USER_DETAILS9")
public class UserDetails9 {

	@Id @GeneratedValue(strategy=AUTO)
	private int userId;
	
	@Column(name="USER_NAME")
	private String userName;
	
	@ManyToMany
	@JoinTable( name="USER_VEHICAL"	,joinColumns = @JoinColumn(name="USER_ID"),
	inverseJoinColumns=@JoinColumn(name="VEHICAL_ID"))

	private Collection<Vehical9> vehical=new ArrayList<Vehical9>();
	

	
	

	public int getUserId() {
		return userId;
	}

	public Collection<Vehical9> getVehical() {
		return vehical;
	}

	public void setVehical(Collection<Vehical9> vehical) {
		this.vehical = vehical;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
		
	
	
	
}