package org.java.dto;

import static javax.persistence.GenerationType.AUTO;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;

@Entity
@Table(name = "USER_DETAILS_TWO")
public class UserDetails2 {

	@Id @GeneratedValue(strategy=AUTO)
	private int userId;
	
	@Column(name="USER_NAME")
	private String userName;
	
	@ElementCollection
	@JoinTable(name="USER_ADDRESS",joinColumns=@JoinColumn(name="USER_ID"))
	private Set<Address2> listofAddress=new HashSet<Address2>();


	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

		
	public Set<Address2> getListofAddress() {
		return listofAddress;
	}

	public void setListofAddress(Set<Address2> listofAddress) {
		this.listofAddress = listofAddress;
	}
	
	
}