package org.java.dto;

import static javax.persistence.GenerationType.AUTO;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USER_DETAILS8")
public class UserDetails8 {

	@Id @GeneratedValue(strategy=AUTO)
	private int userId;
	
	@Column(name="USER_NAME")
	private String userName;
	
	@OneToMany(mappedBy="userdetails8")
	private Collection<Vehical8> vehical=new ArrayList<Vehical8>();
	

	
	

	public int getUserId() {
		return userId;
	}

	public Collection<Vehical8> getVehical() {
		return vehical;
	}

	public void setVehical(Collection<Vehical8> vehical) {
		this.vehical = vehical;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
		
	
	
	
}