package org.java.dto;

import static javax.persistence.GenerationType.AUTO;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "USER_DETAILS10")
public class UserDetails10 {

	@Id @GeneratedValue(strategy=AUTO)
	private int userId;
	
	@Column(name="USER_NAME")
	private String userName;
	
	@OneToMany(cascade=CascadeType.PERSIST)
	private Collection<Vehical10> vehical=new ArrayList<Vehical10>();
	

	
	

	public int getUserId() {
		return userId;
	}

	public Collection<Vehical10> getVehical() {
		return vehical;
	}

	public void setVehical(Collection<Vehical10> vehical) {
		this.vehical = vehical;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
		
	
	
	
}