package org.java.dto;

import static javax.persistence.GenerationType.AUTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;

import org.hibernate.annotations.CollectionId;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "USER_DETAILS3")
public class UserDetails3 {

	@Id @GeneratedValue(strategy=AUTO)
	private int userId;
	
	@Column(name="USER_NAME")
	private String userName;
	
	@ElementCollection
	@JoinTable(name="USER_ADDRESS",joinColumns=@JoinColumn(name="USER_ID"))
	
	@GenericGenerator(name = "helo-gen", strategy = "hilo")
	@CollectionId(columns = { @Column(name="ADDRESS_ID") }, generator = "helo-gen", type = @Type(type="long"))
	private Collection<Address2> listofAddress=new ArrayList<Address2>();


	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Collection<Address2> getListofAddress() {
		return listofAddress;
	}

	public void setListofAddress(Collection<Address2> listofAddress) {
		this.listofAddress = listofAddress;
	}

		
	
	
	
}