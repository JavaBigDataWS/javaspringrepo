package com.citi.services.springbootrestservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class SpringbootRestservicesApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringbootRestservicesApplication.class, args);
	}

}
