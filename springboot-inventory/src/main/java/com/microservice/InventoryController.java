package com.microservice;

import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InventoryController {
	@GetMapping("/inventoryList")
	public ResponseEntity<List<String>> getBookList() {
		return ResponseEntity.ok(InventoryService.getBookList());
	}
}