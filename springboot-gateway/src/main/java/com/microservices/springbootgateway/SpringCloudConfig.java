package com.microservices.springbootgateway;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringCloudConfig {

	@Bean
	public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
		return builder.routes()
				.route(r -> r.path("/order/**")
						.filters(f -> f.addRequestHeader("first-request", "first-request-header")
								.addResponseHeader("first-response", "first-response-header"))
						.uri("http://localhost:9998/")).build();
	}

}