package com.test.spring.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DogsRepository extends CrudRepository<Dog, Long> {
	
	 List<Dog> findByAge(int age);  //Custom Method 
	 
	 @Query("SELECT f FROM Dog f WHERE LOWER(f.name) = LOWER(:name)")
	 Dog retrieveByName(@Param("name") String name);
	 
	 
	/* @Query(value="{'name' : ?0},  'age' :    {'$in' :[?1, 'ALL']}    ")
	 Dog retrieveListByName(String name, int age);  Support only in Mongo Repo*/
}