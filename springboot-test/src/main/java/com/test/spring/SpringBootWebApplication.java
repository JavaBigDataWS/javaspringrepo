package com.test.spring;
 
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

import com.test.spring.rest.CmdbProperties;
 
@SpringBootApplication
@EnableAuthorizationServer
public class SpringBootWebApplication {
 
    private static Logger logger = LoggerFactory.getLogger(SpringBootWebApplication.class);
 
    @Autowired
    private CmdbProperties cmdbProperties;
    
 
    public static void main(String[] args) throws Exception {
        SpringApplication.run(SpringBootWebApplication.class, args);
    }
 
    @PostConstruct
    public void init() {
 
        System.out.println(cmdbProperties.toString());
    }
}