package com.spring.ioc;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;

public class DrawingApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("SpringClasspath.xml");
		Triangle triangleApp=(Triangle) applicationContext.getBean("triangle");
		System.out.println(triangleApp.getPointA().getX()+"&"+triangleApp.getPointA().getY());

	}

}
