package com.spring.ioc;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class Triangle implements ApplicationContextAware , BeanNameAware,BeanFactoryAware{
	
	private Point pointA;
	private Point pointB;
	private Point pointC;
	private ApplicationContext context=null;
	
	public Point getPointA() {
		return pointA;
	}


	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}


	public Point getPointB() {
		return pointB;
	}


	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}


	public Point getPointC() {
		return pointC;
	}


	public void setPointC(Point pointC) {
		this.pointC = pointC;
	}


	public void draw(){
		System.out.println("Triangle Draw");
	}


	@Override
	public void setApplicationContext(ApplicationContext context)throws BeansException {
		this.context=context;
		System.out.println("context"+context);
		
	}


	@Override
	public void setBeanName(String beanName) {
		System.out.println(beanName);
		
	}


	@Override
	public void setBeanFactory(BeanFactory beanfactory) throws BeansException {
		System.out.println("Check Is Singleton :"+beanfactory.isSingleton("pointA"));
		System.out.println("Check Is ProttoType :"+beanfactory.isPrototype("pointA"));
	}
}
