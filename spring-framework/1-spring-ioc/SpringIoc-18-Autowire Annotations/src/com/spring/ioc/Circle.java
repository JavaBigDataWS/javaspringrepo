package com.spring.ioc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;


public class Circle implements Shape{

	
	private Point center;
	
	public Point getCenter() {
		return center;
	}

	@Autowired
	@Qualifier("pointA")
	public void setCenter(Point center) {
		this.center = center;
	}

	@Override
	public void draw() {
		System.out.println("Circle Drawn X "+center.getX()+" Y "+center.getY());
		
	}

}
