package com.spring.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class JdbcDaoImpl {

	
	@Autowired
	private SimpleJdbcTemplate simplejdbctemplate;
	
	
	
	/*
	 * As long as any element is coming after id no problem but once element come before id we have to rearrange whole array so use 
	 * simplejdbctemplate but again if you want quetion mark subtitution and parameter both then Use SimpleJdbcTempolte
	 * 
	 */
	


	public SimpleJdbcTemplate getSimplejdbctemplate() {
		return simplejdbctemplate;
	}



	public void setSimplejdbctemplate(SimpleJdbcTemplate simplejdbctemplate) {
		this.simplejdbctemplate = simplejdbctemplate;
	}



	public 	int insertCircle (Circle cir){
		String sql="insert into circle (name,id)values (:name,:id)";
		SqlParameterSource sqlparametersource=new MapSqlParameterSource("name",cir.getName()).addValue("id", cir.getId());
		return simplejdbctemplate.update(sql,sqlparametersource); 
	}
	
	public 	int simpleinsertCircle (Circle cir){
		String sql="insert into circle (name,id)values (?,?,?)";
		return simplejdbctemplate.update(sql, new Object[]{cir.getName(),cir.getId()});
	}
	

}
