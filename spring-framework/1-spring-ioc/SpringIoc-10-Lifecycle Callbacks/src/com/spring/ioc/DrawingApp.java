package com.spring.ioc;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DrawingApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		AbstractApplicationContext applicationContext=new ClassPathXmlApplicationContext("SpringClasspath.xml");
		applicationContext.registerShutdownHook();//Applicable for desktop Application
		Triangle triangleApp=(Triangle) applicationContext.getBean("triangle1");
		System.out.println(triangleApp.getPoints());
		

	}

}
