package com.spring.ioc;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;


@Component
public class Circle implements Shape{

	@Autowired
	private MessageSource messageSource;
	
	
	public MessageSource getMessageSource() {
		return messageSource;
	}

	
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}


	private Point center;
	
	public Point getCenter() {
		return center;
	}

	@Resource(name="pointC")
	public void setCenter(Point center) {
		this.center = center;
	}

	@Override
	public void draw() {
		System.out.println(this.messageSource.getMessage("text", new Object[]{center.getX(),center.getY()},"defalut grettting",null));
		
	}

	@PostConstruct
	public void myInitailized() throws Exception {
		System.out.println(" My afterPropertiesSet is gonna to call");
		
	}


	@PreDestroy
	public void myDestroy() throws Exception {
		System.out.println("My destroy is gonna call");
		
	}
}
