package com.spring.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class JdbcDaoImpl {

	private static final RowMapper CircleMapper = null;
	@Autowired
	private JdbcTemplate JdbcTemplate; 
	
	
	public JdbcTemplate getJdbcTemplate() {
		return JdbcTemplate;
	}


	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.JdbcTemplate = jdbcTemplate;
	}

	
	public int getCircleCount(){
		String sql="select count(*) from temp";
		return JdbcTemplate.queryForInt(sql);
	}

	public String getCircleName(int id){
		String sql="select name from table where Id= ?";
		return JdbcTemplate.queryForObject(sql,new Object[]{id},String.class);
	}
	
	public Circle getCircleById (int id){
		String sql="select * from table where Id= ?";
		return JdbcTemplate.queryForObject(sql,new Object[]{id},Circle.class);
	}
	
	public Circle getCircle (int id){
		String sql="select * from table where Id= ?";
		return JdbcTemplate.queryForObject(sql,new Object[]{id},new CircleMapper());
	}
	
	public 	List<Circle> getCircleList (){
		String sql="select * from table where Id= ?";
		return JdbcTemplate.query(sql,new CircleMapper());
	}
	
	public 	int insertCircle (Circle cir){
		String sql="insert into circle (name,id)values (?,?,?)";
		return JdbcTemplate.update(sql, new Object[]{cir.getName(),cir.getId()});
	}
	
	public 	int deleteCircle (Circle cir){
		String sql="delete from circle where name=? and id=?";
		return JdbcTemplate.update(sql, new Object[]{cir.getName(),cir.getId()});
	}
	
	public 	void createCircle(){
		String sql="create table triangle (name varchar(20),id integer )";
		JdbcTemplate.execute(sql);
	}
	
	public 	void dropCircle(){
		String sql="drop table triangle";
		JdbcTemplate.execute(sql);
	}
	
	
	
	private static final class CircleMapper implements RowMapper<Circle>{

		@Override
		public Circle mapRow(ResultSet rs, int rownum) throws SQLException {
			Circle ci=new Circle();
			ci.setName(rs.getString(1));
			return null;
		}
		
	}
	
	
}
