package com.spring.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

@Component
public class JdbcDaoImpl {

	
	@Autowired
	private NamedParameterJdbcTemplate namedparameterjdbctemplate;
	
	
	
	/*
	 * As long as any element is coming after id no problem but once element come before id we have to rearrange whole array so use 
	 * NamedParameterJdbcTemplate but again if you want quetion mark subtitution and parameter both then Use SimpleJdbcTempolte
	 * 
	 */
	
	public NamedParameterJdbcTemplate getNamedparameterjdbctemplate() {
		return namedparameterjdbctemplate;
	}



	public void setNamedparameterjdbctemplate(
			NamedParameterJdbcTemplate namedparameterjdbctemplate) {
		this.namedparameterjdbctemplate = namedparameterjdbctemplate;
	}



	public 	int insertCircle (Circle cir){
		String sql="insert into circle (name,id)values (:name,:id)";
		SqlParameterSource sqlparametersource=new MapSqlParameterSource("name",cir.getName()).addValue("id", cir.getId());
		return namedparameterjdbctemplate.update(sql,sqlparametersource); 
	}
	

}
