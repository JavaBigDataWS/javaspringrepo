package com.spring.ioc;

import java.util.List;

public class Triangle {
	
	private List<Point> points;
	
	

	public Triangle(List<Point> points) {
		System.out.println("using constroctor");
		this.points = points;
	}



	public List<Point> getPoints() {
		return points;
	}



	public void setPoints(List<Point> points) {
		this.points = points;
	}



	public void draw(){
		System.out.println("Triangle Draw");
	}
}
