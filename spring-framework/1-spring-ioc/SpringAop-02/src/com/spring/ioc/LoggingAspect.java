package com.spring.ioc;

import javax.sql.rowset.Joinable;

import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;


@Aspect
public class LoggingAspect {
	
	
	/*@Before("execution(public String getName())")
	public void LoggingAdvice() {
		System.out.println("Advice Run get Method is called");
	}*/
	
	/*@Before("execution(public String com.spring.ioc.Circle.getName())")
	public void LoggingAdvice() {
		System.out.println("Advice Run get Method is called");
	}*/
	
	/*@Before("execution(public * get*())")
	public void LoggingAdvice() {
		System.out.println("Advice Run get Method is called");
	}*/
	
	/*
	 * get(..) It can be zero argument or any number of argument
	 * get(*) It can be not be zero argument
	 * get() It is valid for only zero argument
	 */
	
	/*@Before("execution( * get*(..))")
	public void LoggingAdvice() {
		System.out.println("Advice Run get Method is called");
	}*/
	
	/*@Before("execution( * get*(*))")
	public void LoggingAdvice() {
		System.out.println("Advice Run get Method is called");
	}*/
	
	/*@Before("pointcutLoggingAdvice()")
	public void LoggingAdvice() {
		System.out.println("Advice Run get Method is called");
	}
	
	@Before("pointcutLoggingAdvice()")
	public void SecondLoggingAdvice() {
		System.out.println("Advice SecondLoggingAdvice Run get Method is called");
	}
	*/
	
	/*@Before("pointcutAllCircleMethodLoggingAdvice()")
	public void LoggingAdvice() {
		System.out.println("Advice Run get Method is called");
	}*/

	/*@Before("pointcutAllCircleMethodWithinLoggingAdvice()&& ")
	public void LoggingAdviceWithin() {
		System.out.println("Advice Run get Method is called");
	}
	*/
	/*@Pointcut("execution(* * com.spring.ioc.Circle.*(..))")
	public void pointcutAllCircleMethodLoggingAdvice() {
	} */
						//or
	
	/*@Pointcut("within(com.spring.ioc.Circle)")
	public void pointcutAllCircleMethodWithinLoggingAdvice() {
	}
	
	@Pointcut("within(com.spring.ioc.*)")
	public void pointcutAllClassesMethodWithinLoggingAdvice() {
	}*/
	
	/*
	 * within(com.spring.ioc..* If  sub package put ..*)
	 */
	
	/*@Pointcut("within(com.spring.ioc.*)") 
	public void pointcutAllClassesSubPackageMethodWithinLoggingAdvice() {
	}*/
	
	/*@Before("pointcutAllCircleMethodWithinLoggingAdvice() && pointcutAllClassesMethodWithinLoggingAdvice()")
	public void LoggingAdviceWithin() {
		System.out.println("Advice Run get Method is called");
	}
	*/
	
	//********************************************Advance Before Advice********************************************************************
	
	/*
	 * In below advice we r giving message like all getter run but as per Pointcut it will print for setter also
	 */
	
	
	/*@Before("allCircleMethods()")
	public void LoggeingAdvice(JoinPoint jointPoint){
		System.out.println(jointPoint.toString() +"Method executed");
		Circle c=(Circle) jointPoint.getTarget();
		System.out.println(c.getName()+"Name of Object");
	}
	
	@Before("args(String)")
	public void LoggeingSetterAdvice(){
		System.out.println("Setter method got invoekd");
	}
	
	@Before("args(name)")
	public void LoggeingSetterAdviceWithName(String name){
		System.out.println("Setter method got invoekd" + name);
	}
	
	
	@Pointcut("execution (* get*(..))")
	public void allGetters(){
	}
	
	@Pointcut("within(com.spring.ioc.Circle)")
	public void allCircleMethods(){
	}*/
	
}
