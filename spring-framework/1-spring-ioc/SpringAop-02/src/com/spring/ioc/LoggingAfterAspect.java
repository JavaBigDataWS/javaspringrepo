package com.spring.ioc;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;


@Aspect
public class LoggingAfterAspect {
	
	/*@After("LoggerAfterAdvoicePointCut()")
	public void LoggerAfterAdvoice() {
		System.out.println("After Advise is called");
	}
	
	@AfterReturning(pointcut="LoggerAfterAdvoicePointCut()",returning="returnString")
	public void LoggerAfterAdvoice(String returnString) {
		System.out.println("After Advise is called and returnString is name of Object"+returnString);
	}
	

	@AfterThrowing(pointcut="LoggerAfterAdvoicePointCut()",throwing="ex")
	public void LoggerAfterAdvoice(Exception ex) {
		System.out.println("@AfterThrowing is called"+ex);
	}*/
	/*
	@Around("LoggerAfterAdvoicePointCut()")
	public void LoggerArpoundAdvoice(ProceedingJoinPoint jp) {
		
		try {
			System.out.println("Before proceed");
			jp.proceed();
			System.out.println("After proceed");
		} catch (Throwable e) {
			System.out.println("Exception proceed");
			e.printStackTrace();
		}
		System.out.println("Finnaly proceed");
	}*/
	
	@Pointcut("execution(public * set*(..))")
	public void LoggerAfterAdvoicePointCut() {
		
	}

}