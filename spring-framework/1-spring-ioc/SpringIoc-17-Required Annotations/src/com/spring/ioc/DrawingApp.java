package com.spring.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DrawingApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("SpringClasspath.xml");
		Shape shape=(Shape) applicationContext.getBean("circle");
		shape.draw();
		
		
	

	}

}
