package com.spring.jdbc;

import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;

public class SimpleJdbdDaoImple extends SimpleJdbcDaoSupport {
	
	public int getCircleCount(){
		String sql="select count(*) from temp";
		return this.getJdbcTemplate().queryForInt(sql);
	}

}
