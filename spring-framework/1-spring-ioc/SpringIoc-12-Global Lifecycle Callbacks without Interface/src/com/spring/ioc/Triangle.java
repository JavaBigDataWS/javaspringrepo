package com.spring.ioc;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class Triangle{
	
	private List<Point> points;






public List<Point> getPoints() {
	return points;
}



public void setPoints(List<Point> points) {
	this.points = points;
}



public void draw(){
	System.out.println("Triangle Draw");
}



public void myInit() throws Exception {
	System.out.println("afterPropertiesSet is gonna to call");
	
}



public void myDestroy() throws Exception {
	System.out.println("destroy is gonna call");
	
}
}
