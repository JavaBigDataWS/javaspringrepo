package com.spring.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("SpringClasspath.xml");
		ShapeService ss=applicationContext.getBean("shapeserivce",ShapeService.class);
		System.out.println(ss.getCircle().getName());
		

	}

}
