package com.spring.ioc;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DrawingApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		AbstractApplicationContext applicationContext=new ClassPathXmlApplicationContext("SpringClasspath.xml");
		applicationContext.registerShutdownHook();
		Shape shape=(Shape) applicationContext.getBean("circle");
		shape.draw();
		
		
	

	}

}
