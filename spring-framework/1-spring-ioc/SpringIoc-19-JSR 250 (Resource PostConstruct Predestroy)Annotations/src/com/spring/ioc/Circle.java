package com.spring.ioc;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;


public class Circle implements Shape{

	
	private Point center;
	
	public Point getCenter() {
		return center;
	}

	@Resource(name="pointC")
	public void setCenter(Point center) {
		this.center = center;
	}

	@Override
	public void draw() {
		System.out.println("Circle Drawn X "+center.getX()+" Y "+center.getY());
		
	}

	@PostConstruct
	public void myInitailized() throws Exception {
		System.out.println(" My afterPropertiesSet is gonna to call");
		
	}


	@PreDestroy
	public void myDestroy() throws Exception {
		System.out.println("My destroy is gonna call");
		
	}
}
