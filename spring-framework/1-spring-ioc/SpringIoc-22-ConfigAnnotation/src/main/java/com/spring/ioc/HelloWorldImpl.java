package com.spring.ioc;

public class HelloWorldImpl implements HelloWorld {

	public void printHelloWorld(String msg) {

		System.out.println("Hello I from HelloWorld : " + msg);
	}

}