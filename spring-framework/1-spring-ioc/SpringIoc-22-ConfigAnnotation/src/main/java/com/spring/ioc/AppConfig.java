package com.spring.ioc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
	
    @Bean(name="helloworldimpl")
    public HelloWorld helloWorld() {
        return new HelloWorldImpl();
    }
    
    @Bean(name="helloBean")
    public HelloWorld helloWorld1() {
        return new HelloWorldImpl();
    }
    
    @Bean(name="helloIndiaBean")
    public HelloWorld helloIndia() {
        return new HelloIndiaImpl();
    }
    
    @Bean(name="helloIndiaBean1")
    public HelloWorld helloIndiaBean() {
        return new HelloIndiaImpl();
    }
    
  
	
}