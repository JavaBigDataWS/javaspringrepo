package com.spring.ioc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;

@Scope("prototype")
public class HelloIndiaImpl implements HelloWorld {
	
	@Autowired(required=false)
	@Qualifier("helloworldimpl")
	HelloWorldImpl  helloworldimpl;
	

	public void printHelloWorld(String msg) {
		// TODO Auto-generated method stub
		helloworldimpl.printHelloWorld(msg);
	}

}
