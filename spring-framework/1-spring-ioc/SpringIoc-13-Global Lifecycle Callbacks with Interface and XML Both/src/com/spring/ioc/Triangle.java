package com.spring.ioc;

import java.util.List;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Triangle implements  InitializingBean,DisposableBean{
	
	private List<Point> points;






public List<Point> getPoints() {
	return points;
}



public void setPoints(List<Point> points) {
	this.points = points;
}



public void draw(){
	System.out.println("Triangle Draw");
}



@Override
public void afterPropertiesSet() throws Exception {
	System.out.println("afterPropertiesSet is gonna to call");
	
}



@Override
public void destroy() throws Exception {
	System.out.println("destroy is gonna call");
	
}

public void myInit() throws Exception {
	System.out.println("MyafterPropertiesSet is gonna to call");
	
}



public void myDestroy() throws Exception {
	System.out.println("Mydestroy is gonna call");
	
}
}
