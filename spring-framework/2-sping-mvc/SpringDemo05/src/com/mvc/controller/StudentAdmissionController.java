package com.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StudentAdmissionController{


	
	@RequestMapping(value="/admissionform.html")
	protected ModelAndView getAdmissionform() {
		ModelAndView modelandview=new ModelAndView("AdmissionForm");
		return modelandview;
	}

	@RequestMapping(value="/submitadmissionform.html",  method=RequestMethod.POST)
	protected ModelAndView submitAdmissionform(@RequestParam("studentName") String studentName,@RequestParam("studentHobby") String studentHobby) {
		ModelAndView modelandview=new ModelAndView("AdmissionSuccessForm");
		return modelandview;
			
			
		}
	}
  
