package com.mvc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.standard.Media;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.ibm.msg.client.commonservices.componentmanager.Component;


@RestController
public class StudentAdmissonRestController {
	
	@RequestMapping(value="/getStudents/",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Student> getStudentList(){
		
		ArrayList<Student> list=new ArrayList<Student>();
		Student student=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		
		Student student1=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		
		list.add(student);
		list.add(student1);
		
		
		return list;
		
	}
	
	
	@RequestMapping(value="/getStudents/{name}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public Student getStudent(@PathVariable("name") String name){
		
		Student student=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		System.out.println("Name of Student :"+name);
		return student;
		
	}
	
	@RequestMapping(value="/getStudentEntity/{name}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Student> getStudentEntity(@PathVariable("name") String name){
		
		Student student=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		System.out.println("Name of Student :"+name);
		return new ResponseEntity<Student>(student,HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value="/getStudentHeaders/{name}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Student> getStudentHeaders(@PathVariable("name") String name){
		
		HttpHeaders headers=new HttpHeaders();
		headers.add("Name", "ArunKumar");
		headers.add("Hobby", "Coding");
		Student student=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		System.out.println("Name of Student :"+name);
		return new ResponseEntity<Student>(student,headers,HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value="/updateStudent/{name}",method=RequestMethod.PUT,produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public boolean updateStudent(@PathVariable("name") String name ,@RequestBody Student studen){
		
		Student student=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		System.out.println("Name of Student :"+name);
		return true;
		
	}
	
	@RequestMapping(value="/createStudent",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Student>  createStudent(@RequestBody Student studen){
		
		HttpHeaders headers=new HttpHeaders();
		//headers.add("Location",ComponentsBuilder.
		Student student=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		return new ResponseEntity<Student>(student,headers,HttpStatus.CREATED);
		
	}
	
	@RequestMapping(value="/deleteStudent/{name}",method=RequestMethod.DELETE)
	public ResponseEntity<Boolean>  deleteStudent(@PathVariable("name") String name ){
		
		HttpHeaders headers=new HttpHeaders();
		//headers.add("Location",ComponentsBuilder.
		Student student=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		return new ResponseEntity<Boolean>(true,headers,HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/deleteAllStudent",method=RequestMethod.DELETE)
	public ResponseEntity<Boolean>  deleteAllStudent(){
		
		HttpHeaders headers=new HttpHeaders();
		//headers.add("Location",ComponentsBuilder.
		//
		//Delete all student from Database
		return new ResponseEntity<Boolean>(true,headers,HttpStatus.OK);
		
	}
	
}
