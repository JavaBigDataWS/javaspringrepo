package com.mvc.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StudentAdmissionController{

	/*@ResponseBody
	@RequestMapping(value="/getStudents",method=RequestMethod.GET)
	public List<Student> getStudentList(){
		
		ArrayList<Student> list=new ArrayList<Student>();
		Student student=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		
		Student student1=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		
		list.add(student);
		list.add(student1);
		
		
		return list;
		
	}
	
	
	@ResponseBody
	@RequestMapping(value="/getStudents/{name}",method=RequestMethod.GET)
	public Student getStudent(@PathVariable("name") String name){
		
		Student student=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		System.out.println("Name of Student :"+name);
		return student;
		
	}
	*/
		
	
	
	
	
	
	
	/*
	 * InitBinder will be consulted before doing any DataBinding
	 */
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy***MM****dd**");
		binder.registerCustomEditor(Date.class, "studenyDOB", new CustomDateEditor(dateFormat, false));
		binder.registerCustomEditor(String.class, "studentName", new StudentPropertyEditor());
	}
	
	
	@RequestMapping(value="/admissionform")
	protected ModelAndView getAdmissionform() throws IOException {
		ModelAndView modelandview=new ModelAndView("AdmissionForm");
		return modelandview;	
		}
	
	@ModelAttribute
	protected void addcommonObject(Model model) {
		model.addAttribute("messgae", "Welcome to Spring MVC Using Model");
		}
	
	
	/*
	 *When @Valid along with @ModelAttribute will be found then Only Spring will consider all annotation put on Student Class for validation while DataBinding
	 *Implementation found in  Hibernate Validator  http://hibernate.org/validator/download
	 */
	
	@RequestMapping(value="/submitadmissionform",  method=RequestMethod.POST)
	protected ModelAndView submitAdmissionform(@Valid @ModelAttribute("student1") Student student,BindingResult bindingresult)  {
		if(bindingresult.hasErrors()){
			
			ModelAndView modelandview=new ModelAndView("AdmissionForm");
			return modelandview;	
		}
		
		ModelAndView modelandview=new ModelAndView("AdmissionSuccessForm");
		return modelandview;
		}
	
	}
  
