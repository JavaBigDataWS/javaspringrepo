package com.mvc.controller;

import java.util.ArrayList;
import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


public class Student {
	
	
	/*
	 *  <!-- support JSR303 annotation if JSR 303 validation present on classpath <mvc:annotation-driven />/
	 */
	
	@Size(min=2,max=5, message="aaa")
	private String studentHobby;
	//@Pattern(regexp="[^0.9]*")
	private String studentName;
	@Max(99999)
	private Long studentMobile;
	@Past
	private Date studentDOB;
	private ArrayList<String> studentskills;
	private Address address;
	
	
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getStudentHobby() {
		return studentHobby;
	}
	public void setStudentHobby(String studentHobby) {
		this.studentHobby = studentHobby;
	}
	public Long getStudentMobile() {
		return studentMobile;
	}
	public void setStudentMobile(Long studentMobile) {
		this.studentMobile = studentMobile;
	}
	public Date getStudentDOB() {
		return studentDOB;
	}
	public void setStudentDOB(Date studentDOB) {
		this.studentDOB = studentDOB;
	}
	public ArrayList<String> getStudentskills() {
		return studentskills;
	}
	public void setStudentskills(ArrayList<String> studentskills) {
		this.studentskills = studentskills;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
}
