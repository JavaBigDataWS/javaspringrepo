package com.mvc.controller;

import java.beans.PropertyEditorSupport;

public class StudentPropertyEditor extends PropertyEditorSupport{
	
	
	@Override
	public void setAsText(String studentName) throws IllegalArgumentException {
		
		if(studentName.contains("Mr.")||studentName.contains("Ms.")){

			super.setAsText(studentName);
			
		}else {
			studentName="Ms."+studentName;
			setValue(studentName);
		}
		}
}