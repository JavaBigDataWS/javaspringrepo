package com.mvc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.standard.Media;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.ibm.msg.client.commonservices.componentmanager.Component;


@RestController
public class StudentAdmissonRestController {
	
	@RequestMapping(value="/user/",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Student> getStudentList(){
		
		ArrayList<Student> list=new ArrayList<Student>();
		Student student=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		
		Student student1=new Student();
		student.setStudentName("Arun");
		student.setStudentHobby("Coding");
		
		list.add(student);
		list.add(student1);
		
		
		return list;
		
	}
	
}
