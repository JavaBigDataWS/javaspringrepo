package com.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController{


	
	@RequestMapping("/welcome")
	protected ModelAndView welcome() {
		ModelAndView modelandview=new ModelAndView("HelloPage");
		modelandview.addObject("welcomeMessage", "Hi Welocome to MVC");
		return modelandview;
		
	}

}
