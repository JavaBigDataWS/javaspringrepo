package com.mvc.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StudentAdmissionController{


	
	@RequestMapping(value="/admissionform.html")
	protected ModelAndView getAdmissionform() {
		ModelAndView modelandview=new ModelAndView("AdmissionForm");
	
		return modelandview;
	}
	
	@ModelAttribute
	protected void addcommonObject(Model model) {
		model.addAttribute("messgae", "Welcome to Spring MVC Using Model");
	}
	
	@RequestMapping(value="/submitadmissionform.html",  method=RequestMethod.POST)
	protected ModelAndView submitAdmissionform(@RequestParam Map<String, String> requestParamMap)  {
		String studentName=requestParamMap.get("studentName");
		String studentHobby=requestParamMap.get("studentHobby");
		System.out.println("***************"+studentHobby+studentName); 
		ModelAndView modelandview=new ModelAndView("AdmissionSuccessForm");
		return modelandview;
			
			
		}
	}
  
