package com.mvc.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sun.xml.internal.org.jvnet.staxex.NamespaceContextEx.Binding;

@Controller
public class StudentAdmissionController{


	/*
	 * InitBinder will be consulted before doing any DataBinding
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder){
		binder.setDisallowedFields(new String[] {"studentMobile"});
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy***MM****dd**");
		binder.registerCustomEditor(Date.class, "studenyDOB", new CustomDateEditor(dateFormat, false));
		binder.registerCustomEditor(String.class, "studentName", new StudentPropertyEditor());
	}
	
	
	@RequestMapping(value="/admissionform")
	protected ModelAndView getAdmissionform() {
		ModelAndView modelandview=new ModelAndView("AdmissionForm");
		return modelandview;	
		}
	
	@ModelAttribute
	protected void addcommonObject(Model model) {
		model.addAttribute("messgae", "Welcome to Spring MVC Using Model");
		}
	
	@RequestMapping(value="/submitadmissionform",  method=RequestMethod.POST)
	protected ModelAndView submitAdmissionform(@ModelAttribute("student1") Student student,BindingResult bindingresult)  {
		if(bindingresult.hasErrors()){
			
			ModelAndView modelandview=new ModelAndView("AdmissionForm");
			return modelandview;	
		}
		
		ModelAndView modelandview=new ModelAndView("AdmissionSuccessForm");
		return modelandview;
		}
	}
  
