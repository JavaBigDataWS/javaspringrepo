package com.mvc.controller;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

	/*
	 * All the Exception in this class will be applicable to all Controller beacause of @ControllerAdvice annotation ,same can achveied by configuring all exception and respected viewName in spring.dispatcher-servlet xml 
	*/


@ControllerAdvice
public class GlobalExceptionHandlerMethods {
	

	@ExceptionHandler(value=NullPointerException.class)
	public String nullPointerException(Exception e){
		
		System.out.println(e);
		
		return "NullPointerException";
		}
	
	@ExceptionHandler(value=IOException.class)
	public String IOException(Exception e){
		
		System.out.println(e);
		
		return "IOException";
		}
	
	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value=Exception.class)
	public String Exception(Exception e){
		
		System.out.println(e);
		
		return "Exception";
		}
	

}
