package com.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StudentAdmissionController{


	
	@RequestMapping(value="/admissionform")
	protected ModelAndView getAdmissionform() {
		ModelAndView modelandview=new ModelAndView("AdmissionForm");
		return modelandview;	
		}
	
	@ModelAttribute
	protected void addcommonObject(Model model) {
		model.addAttribute("messgae", "Welcome to Spring MVC Using Model");
		}
	
	@RequestMapping(value="/submitadmissionform",  method=RequestMethod.POST)
	protected ModelAndView submitAdmissionform(@ModelAttribute("student1") Student student)  {
		ModelAndView modelandview=new ModelAndView("AdmissionSuccessForm");
		return modelandview;
		}
	}
  
