package com.mvc.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController{


	
	@RequestMapping("/welcome/{place}/{Hotel}")
	protected ModelAndView welcome(@PathVariable("place") String place,@PathVariable("Hotel") String Hotel) {
		ModelAndView modelandview=new ModelAndView("HelloPage");
		modelandview.addObject("welcomeMessage", "Hi Welocome to "+place +" at " +Hotel);
		return modelandview;
		
	}
	
	@RequestMapping("/hello/{place}/{hotel}")
	protected ModelAndView hello(@PathVariable Map<String, String> Variablemap) {
		String place=(String)Variablemap.get("place");
		String hotel=(String)Variablemap.get("hotel");
		System.out.println(" place "+place+" Hotel "+hotel);
		ModelAndView modelandview=new ModelAndView("HelloPage");
		modelandview.addObject("welcomeMessage", "Hi Welocome to "+place +" at " +hotel);
		return modelandview;
		
		
	}

}
